<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Handling;

use JetBrains\PhpStorm\Immutable;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
final class MultiTransportMessage
{

}
