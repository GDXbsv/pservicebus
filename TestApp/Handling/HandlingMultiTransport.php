<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Handling;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use JetBrains\PhpStorm\Immutable;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
class HandlingMultiTransport
{
    #[Handle(transportName: 'memory1')]
    public function transport1(MultiTransportMessage $message): void
    {
        return;
    }
    #[Handle(transportName: 'memory2')]
    public function transport2(MultiTransportMessage $message): void
    {
        return;
    }
}
