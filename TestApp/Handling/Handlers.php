<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Handling;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Bus\Handling\MessageHandleContext;

/**
 * @internal
 */
final class Handlers
{
    public string $result = '';

    #[Handle('memory')]
    public function handleMulti1(TestMultiHandlersCommand $command, MessageHandleContext $context): void
    {
        $this->result .= '||' . $command->name;
    }
    #[Handle('memory')]
    public function handleMulti2(TestMultiHandlersCommand $command, MessageHandleContext $context): void
    {
        $this->result .= '||' . $command->name;
    }
    #[Handle('memory')]
    public function __invoke(Test1Command $command, MessageHandleContext $context): void
    {
        $this->result .= '||' . $command->name;
    }
    #[Handle('memory')]
    public function anyNameFunction(Test1Event $event, MessageHandleContext $context): void
    {
        $this->result .= '||' . $event->name;
    }
    #[Handle('memory')]
    public function handle2Event1(Test1Event $event, MessageHandleContext $context): void
    {
        $this->result .= '||' . $event->name;
    }
}
