<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

final class TestsSagaInTimeoutEvent
{
    public ?string $string = 'testSaga';
    public ?string $value = 'secretValue';
}
