<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

class TestSagaCreateCommand
{
    public string $id = 'testSaga';
    public string $string = 'testSagaString';
}
