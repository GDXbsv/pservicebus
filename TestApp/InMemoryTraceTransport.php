<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp;

use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;

final class InMemoryTraceTransport extends InMemoryTransport
{
    public string $name = '';

    public array $envelopesRecorded = [];

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion Still do not know why
     */
    protected function sendingCoroutine(): \Generator
    {
        $parentSending = parent::sendingCoroutine();
        while (true) {
            $envelope = (yield);
            $parentSending->send($envelope);
            if (!$envelope) {
                break;
            }
            $this->envelopesRecorded[] = $envelope;
        }
    }
}
