<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\HandlingExternal;

use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\Replay\Replay;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 *
 * @psalm-import-type ReplayOutput from \GDXbsv\PServiceBus\Message\Replay\Replaying
 */
final class ReplayForExternalEvent
{
    /**
     * @return ReplayOutput
     */
    #[Replay(replayName: 'testReplayExternal')]
    public function anyName(): \Traversable {
        for ($i=1; $i<=5; ++$i) {
            yield new Message(new ExternalOutEvent(), EventOptions::record());
        }
    }
}
