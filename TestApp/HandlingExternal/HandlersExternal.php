<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\HandlingExternal;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Bus\Handling\MessageHandleContext;

/**
 * @internal
 */
final class HandlersExternal
{
    public string $result = '';

    #[Handle('memory')]
    public function handleExternal1(ExternalOutEvent $event, MessageHandleContext $context): void
    {
        $this->result .= '||' . $event->name;
    }
}
