<?php

declare(strict_types=1);

use GDXbsv\PServiceBus\Setup;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBusTestApp\Handling\Handlers;
use GDXbsv\PServiceBusTestApp\Handling\HandlingMultiTransport;
use GDXbsv\PServiceBusTestApp\Handling\ReplayForEvent;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ExternalOutEvent;
use GDXbsv\PServiceBusTestApp\HandlingExternal\HandlersExternal;
use GDXbsv\PServiceBusTestApp\HandlingExternal\InMemoryExternalTransport;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ReplayForExternalEvent;
use GDXbsv\PServiceBusTestApp\InMemoryTraceTransport;
use GDXbsv\PServiceBusTestApp\Saga\CustomSagaFinder;
use GDXbsv\PServiceBusTestApp\Saga\TestSaga;

require_once "../vendor/autoload.php";

$inMemTransport = new InMemoryTransport();
$inMemTrace1Transport = new InMemoryTraceTransport();
$inMemTrace1Transport->name = '1';
$inMemTrace2Transport = new InMemoryTraceTransport();
$inMemTrace2Transport->name = '2';
$bootstrap = new Setup(
    [TestSaga::class, Handlers::class, HandlingMultiTransport::class],
    [
        'memory' => $inMemTransport,
        'memory1' => $inMemTrace1Transport,
        'memory2' => $inMemTrace2Transport,
        'memory-external' => new InMemoryExternalTransport()
    ],
    [Handlers::class => new Handlers(), HandlersExternal::class => new HandlersExternal(), HandlingMultiTransport::class => new HandlingMultiTransport()],
    [ExternalOutEvent::class],
    [ReplayForEvent::class, ReplayForExternalEvent::class],
    [ReplayForEvent::class => new ReplayForEvent(), ReplayForExternalEvent::class => new ReplayForExternalEvent()],
    [CustomSagaFinder::class],
    [CustomSagaFinder::class => new CustomSagaFinder()],
);
$bootstrap->build();
$bootstrap->getApplication()->run();
