#=========================================================================
ARG FROM_BASE
ARG FROM_DEVELOP
#=========================================================================

#=========================================================================
FROM $FROM_BASE as base
#=========================================================================

#=========================================================================
FROM $FROM_DEVELOP as develop
#=========================================================================

#=========================================================================
FROM develop as build

COPY . /srv/www/app
WORKDIR /srv/www/app

RUN rm -f .env.local.php \
    && APP_ENV=prod composer install --no-dev --no-interaction --optimize-autoloader  --classmap-authoritative \
    && APP_ENV=prod bin/console cache:warmup \
    && echo "<?php return[];" > .env.local.php
#=========================================================================

#=========================================================================
FROM base as live

WORKDIR /srv/www/app
COPY --from=build /srv/www/app /srv/www/app
RUN chmod -R 0777 /srv/www/app/var/cache/
#=========================================================================

