#=========================================================================
#https://github.com/docker-library/php/issues/1417
FROM php:8.2-fpm-bullseye as base

COPY ./deploy/containers/app/php.ini /usr/local/etc/php/php.ini
# Install extensions
ENV EXTENSIONS_BUILD_DEPS \
        libicu-dev \
        libzip-dev
ENV PHP_EXTS_BASE \
        intl \
        opcache \
        bcmath \
        zip \
        pcntl
ENV PHP_EXTS_APP \
        pdo \
        pdo_mysql \
        mysqli
RUN apt-get update \
# install deps
    && apt-get install -y --no-install-recommends ${EXTENSIONS_BUILD_DEPS} \
        # extensions
        && docker-php-ext-install \
                ${PHP_EXTS_BASE} \
                ${PHP_EXTS_APP} \
            && docker-php-ext-enable \
                ${PHP_EXTS_BASE} \
                ${PHP_EXTS_APP} \
    # clean up
    && apt-get purge \
        -y --auto-remove \
        -o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
#=========================================================================
