<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Handling;

use GDXbsv\PServiceBusTestApp\HandlingExternal\ExternalOutEvent;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class ExternalEventsTest extends IntegrationTestCase
{
    public function testEventSucceed()
    {
        $bus = $this->bus;
        $bus->trace();
        $bus->publish($event = new ExternalOutEvent());
        self::assertEquals('||ExternalOutEvent', $this->handlersExternal->result);
        $messages = $bus->getEventMessages();
        self::assertCount(1, $messages);
        self::assertEquals('external', $this->inMemExtTransport->envelopesRecorded[0]->headers['type']);
        self::assertEquals('test.external_out_event', $this->inMemExtTransport->envelopesRecorded[0]->headers['name']);
        self::assertEquals('test.external_out_event', $this->inMemExtTransport->envelopesRecorded[0]->headers['name']);
    }
}
