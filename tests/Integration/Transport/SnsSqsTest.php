<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Transport;

use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\Sns\SnsSqsTransport;
use GDXbsv\PServiceBus\Transport\Sqs\SqsTransport;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ExternalOutEvent;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;
use Prewk\Result\Ok;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class SnsSqsTest extends IntegrationTestCase
{
    public function testSendingExternal()
    {
        $queueName = 'test_ext';
        $transport = $this->getTransport($queueName, ['test.external_out_event' => ExternalOutEvent::class]);
        $this->cleanQueues([$queueName]);

        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 3, '0', 0, ['name' => 'test.external_out_event']));
        $sending->finish();

        sleep(2);

        /** @var list<Envelope> $envelopes */
        $envelopes = [];
        $envelopesGenerator = $transport->receive(10);
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->getCurrentEnvelope();
            $envelopesGenerator->sendResult(new Ok(true));
        }

        self::assertCount(1, $envelopes);
        self::assertEquals(['body1'], $envelopes[0]->payload);
    }

    protected function cleanQueues(array $queuesNames): void
    {
        foreach ($queuesNames as $name) {
            $this->getFromQueue($name, 10);
        }
    }

    protected function getFromQueue(string $queueName, int $amount): array
    {
        $messages = [];
        $transport = $this->getTransport($queueName, [], false);
        $gen = $transport->receive($amount);
        while ($gen->valid()) {
            $messages[] = $gen->current();
            $gen->send(new Ok(null));
        }
        return $messages;
    }

    protected function getSqsTransport(string $queueName, int $retries = 1, bool $sync = true): SqsTransport
    {
        $transport = SqsTransport::ofDsn(
            "sqs+http://key:secret@aws/000000000000?region=eu-central-1&queue=$queueName&retries=$retries&waitSeconds=0"
        );
        if ($sync) {
            $transport->sync();
        }

        return $transport;
    }

    protected function getTransport(
        string $topic,
        array $messageMapIn,
        bool $sync = true,
    ): SnsSqsTransport {
        $transport = SnsSqsTransport::ofDsn(
            "sns+http://key:secret@aws/000000000000?region=eu-central-1&topic=$topic",
            $this->getSqsTransport($topic),
            $messageMapIn,
        );
        if ($sync) {
            $transport->register();
            $transport->sync();
        }

        return $transport;
    }
}
