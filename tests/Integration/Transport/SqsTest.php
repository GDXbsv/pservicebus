<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Transport;

use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\Sqs\SqsTransport;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;
use Prewk\Result\Err;
use Prewk\Result\Ok;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class SqsTest extends IntegrationTestCase
{

    public function testSendingInternal()
    {
        $queueName = 'testQueue';
        $transport = $this->getTransport($queueName);
        $this->cleanQueues([$queueName]);


        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 3, '0', 0, []));
        $sending->send(new Envelope(['body2'], 3, '0', 0, []));
        $sending->finish();

        $envelopesGenerator = $transport->receive(6);

        /** @var Envelope $envelopes */
        $envelopes = [];
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->getCurrentEnvelope();
            $envelopesGenerator->sendResult(new Ok(true));
        }

        self::assertCount(2, $envelopes);
        self::assertEquals(['body1'], $envelopes[0]->payload);
        self::assertEquals(['body2'], $envelopes[1]->payload);
    }

    public function testSendingInternalNoPreloadNoBatch()
    {
        $queueName = 'testQueue';
        $transport = $this->getTransport(
            queueName:               $queueName,
            waitSeconds:             0,
            waitBetweenLoopsSeconds: 1,
            messagesBatch:           1,
            preload:                 false
        );
        $this->cleanQueues([$queueName]);


        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 3, '0', 0, []));
        $sending->send(new Envelope(['body2'], 3, '0', 0, []));
        $sending->send(new Envelope(['body3'], 3, '0', 0, []));
        $sending->finish();

        $envelopesGenerator = $transport->receive(6);

        /** @var Envelope $envelopes */
        $envelopes = [];
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->getCurrentEnvelope();
            $envelopesGenerator->sendResult(new Ok(true));
        }

        self::assertCount(3, $envelopes);
        self::assertEquals(['body1'], $envelopes[0]->payload);
        self::assertEquals(['body2'], $envelopes[1]->payload);
        self::assertEquals(['body3'], $envelopes[2]->payload);
    }

    public function testSendingInternalDlq()
    {
        $queueName = 'test_dl';
        $transport = $this->getTransport($queueName, retries: 1);
        $this->cleanQueues([$queueName, "{$queueName}_DL"]);


        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 1, '0', 0, []));
        $sending->send(new Envelope(['body2'], 1, '0', 0, []));
        $sending->finish();

        $envelopesGenerator = $transport->receive(6);

        /** @var Envelope $envelopes */
        $envelopes = [];
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->getCurrentEnvelope();
            $envelopesGenerator->sendResult(new Err(true));
        }
        self::assertCount(2, $envelopes);

        $messages = $this->getFromQueue($queueName, 4);
        self::assertCount(0, $messages);

        $messages = $this->getFromQueue("{$queueName}_DL", 20);
        self::assertCount(2, $messages);
    }


    public function testSendingTimeout()
    {
        $queueName = 'test_timeout';
        $transport = $this->getTransport($queueName, waitSeconds: 1);
        $this->cleanQueues([$queueName]);


        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 0, '0', 2, []));
        $sending->finish();

        $messages = $this->getFromTransport($transport, 1);
        self::assertCount(0, $messages);
        sleep(3);
        $messages = $this->getFromTransport($transport, 1);
        self::assertCount(1, $messages);
    }

    public function testRetriesTimeoutExpression()
    {
        $queueName = 'test_retries_timeout';
        $transport = $this->getTransport($queueName, 3);
        $this->cleanQueues([$queueName, "{$queueName}_DL"]);


        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 3, '0', 0, []));
        $sending->finish();

        $envelopesGenerator = $transport->receive(1);
        /** @var Envelope $envelopes */
        $envelopes = [];
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->getCurrentEnvelope();
            $envelopesGenerator->sendResult(new Err(true));
        }
        self::assertCount(1, $envelopes);
        self::assertEquals('0', $envelopes[0]->retriesTimeoutExpression);
        self::assertEquals('0', $envelopes[0]->headers['retries_timeout_expression']);

        sleep(1);
        $envelopesGenerator = $transport->receive(1);
        /** @var Envelope $envelopes */
        $envelopes = [];
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->getCurrentEnvelope();
            $envelopesGenerator->sendResult(new Err(true));
        }
        self::assertCount(1, $envelopes);
        self::assertEquals('0', $envelopes[0]->retriesTimeoutExpression);
        self::assertEquals('0', $envelopes[0]->headers['retries_timeout_expression']);

        $this->cleanQueues([$queueName]);
    }

    protected function cleanQueues(array $queuesNames): void
    {
        foreach ($queuesNames as $name) {
            $this->getFromQueue($name, 10);
        }
    }

    protected function getFromQueue(string $queueName, int $amount): array
    {
        $messages = [];
        $transport = $this->getTransport($queueName, sync: false, waitSeconds: 1, waitBetweenLoopsSeconds: 1);
        $gen = $transport->receive($amount);
        while ($gen->valid()) {
            $messages[] = $gen->getCurrentEnvelope();
            $gen->sendResult(new Ok(null));
        }
        return $messages;
    }
    protected function getFromTransport(SqsTransport $transport, int $amount): array
    {
        $messages = [];
        $gen = $transport->receive($amount);
        while ($gen->valid()) {
            $messages[] = $gen->getCurrentEnvelope();
            $gen->sendResult(new Ok(null));
        }
        return $messages;
    }

    protected function sendToQueue(string $queueName, object $message): void
    {
        $transport = $this->getTransport($queueName);
        $sending = $transport->sending();
        $sending->send(new Message($message, MessageOptions::record()));
        $sending->finish();
    }

    protected function getTransport(
        string $queueName,
        int $retries = 1,
        bool $sync = true,
        int $visibilityTimeout = 30,
        int $waitSeconds = 20,
        int $waitBetweenLoopsSeconds = 40,
        int $messagesBatch = 10,
        bool $preload = true,
    ): SqsTransport {
        $transport = SqsTransport::ofDsn(
            "sqs+http://key:secret@aws/000000000000?region=eu-central-1&queue=$queueName&retries=$retries&visibilityTimeout=$visibilityTimeout&waitSeconds=$waitSeconds&waitBetweenLoopsSeconds=$waitBetweenLoopsSeconds&messagesBatch=$messagesBatch&preload=$preload"
        );
        if ($sync) {
            $transport->sync();
        }

        return $transport;
    }
}
