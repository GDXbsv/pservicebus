<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\TraceableBus;
use GDXbsv\PServiceBus\Setup;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBusTestApp\Handling\Handlers;
use GDXbsv\PServiceBusTestApp\Handling\HandlingMultiTransport;
use GDXbsv\PServiceBusTestApp\Handling\ReplayForEvent;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ExternalOutEvent;
use GDXbsv\PServiceBusTestApp\HandlingExternal\HandlersExternal;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ReplayForExternalEvent;
use GDXbsv\PServiceBusTestApp\InMemoryTraceTransport;
use GDXbsv\PServiceBusTestApp\Saga\CustomSagaFinder;
use GDXbsv\PServiceBusTestApp\Saga\TestSaga;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class IntegrationTestCase extends TestCase
{
    protected Setup $setup;
    protected Application $application;
    /** @var Bus\TraceableBus */
    protected Bus $bus;
    protected Handlers $handlers;
    protected HandlersExternal $handlersExternal;
    protected InMemoryTransport $inMemTransport;
    protected InMemoryTraceTransport $inMemExtTransport;
    protected InMemoryTraceTransport $inMemTraceTransport1;
    protected InMemoryTraceTransport $inMemTraceTransport2;

    protected function setUp(): void
    {
        parent::setUp();

        $setup = $this->prepareSetup();
        $setup->build();
        $this->application = $setup->getApplication();
        $this->bus = $setup->getBus();
        $this->init();
        $this->setup = $setup;
    }

    protected function prepareSetup(): Setup
    {
        $inMemTransport = new InMemoryTransport();
        $this->inMemTransport = $inMemTransport;
        $inMemTrace1Transport = new InMemoryTraceTransport();
        $inMemTrace1Transport->name = '1';
        $this->inMemTraceTransport1 = $inMemTrace1Transport;
        $inMemTrace2Transport = new InMemoryTraceTransport();
        $inMemTrace2Transport->name = '2';
        $this->inMemTraceTransport2 = $inMemTrace2Transport;
        $inMemExtTransport = new InMemoryTraceTransport();
        $this->inMemExtTransport = $inMemExtTransport;
        $this->handlers = new Handlers();
        $this->handlersExternal = new HandlersExternal();
        $setup = new Setup(
            [TestSaga::class, $this->handlers::class, $this->handlersExternal::class, HandlingMultiTransport::class],
            [
                'memory' => $inMemTransport,
                'memory1' => $inMemTrace1Transport,
                'memory2' => $inMemTrace2Transport,
                'memory-external' => $inMemExtTransport
            ],
            [$this->handlers::class => $this->handlers, $this->handlersExternal::class => $this->handlersExternal, HandlingMultiTransport::class => new HandlingMultiTransport()],
            [ExternalOutEvent::class],
            [ReplayForEvent::class, ReplayForExternalEvent::class],
            [
                ReplayForEvent::class => new ReplayForEvent(),
                ReplayForExternalEvent::class => new ReplayForExternalEvent()
            ],
            [CustomSagaFinder::class],
            [CustomSagaFinder::class => new CustomSagaFinder(),],
            busDecorator: TraceableBus::class
        );

        return $setup;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    protected function init(): void
    {
        $application = $this->application;
        $command = $application->find('p-service-bus:init');
        $commandTester = new CommandTester($command);

        $commandExitCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        static::assertSame(0, $commandExitCode);
    }

    protected function consume(string $from): int
    {
        $application = $this->application;
        $command = $application->find('p-service-bus:transport:consume');
        $commandTester = new CommandTester($command);

        $commandExitCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'transport' => $from,
                '--limit' => 50,
            ]
        );

        return $commandExitCode;
    }
}
