<?php

namespace GDXbsv\PServiceBusTests\Unit\Transport;

use GDXbsv\PServiceBus\Transport\Sqs\SqsTransport;
use PHPUnit\Framework\TestCase;

class SqsTransportTest extends TestCase
{
    public function testDsnParsing()
    {
        $sqs = SqsTransport::ofDsn(
            'sqs+http://key:secret@aws:4100/123456789012?region=eu-west-1&retries=5&tags[name]=value&tags[name2]=value2&queue=QueueName'
        );

        $hydrate = \Closure::bind(
            static function (object $object): array {
                /** @var array<scalar|object> $vars */
                $vars = get_object_vars($object);

                return $vars;
            },
            null,
            $sqs::class
        );

        $values = $hydrate($sqs);
        self::assertSame(5, $values['retries']);
        self::assertSame([
                             "name" => "value",
                             "name2" => "value2",
                         ], $values['tags']);
    }

    public function testDsnParsingWithout()
    {
        $sqs = SqsTransport::ofDsn(
            'sqs+http://aws:4100/123456789012?region=eu-west-1&queue=QueueName'
        );

        $hydrate = \Closure::bind(
            static function (object $object): array {
                /** @var array<scalar|object> $vars */
                $vars = get_object_vars($object);

                return $vars;
            },
            null,
            $sqs::class
        );

        $values = $hydrate($sqs);
        self::assertSame(3, $values['retries']);
        self::assertSame([], $values['tags']);
    }
}
