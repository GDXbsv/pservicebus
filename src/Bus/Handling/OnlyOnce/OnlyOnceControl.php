<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling\OnlyOnce;

use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;

interface OnlyOnceControl
{
    /**
     * @param Message<EventOptions> $message
     */
    public function continue(Message $message): bool;
    /**
     * @param Message<EventOptions> $message
     */
    public function clean(Message $message): bool;
}
