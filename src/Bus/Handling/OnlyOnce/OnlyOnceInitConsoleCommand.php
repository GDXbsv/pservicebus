<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling\OnlyOnce;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'p-service-bus:only-once:init', description: 'Init only once infrastructure.')]
class OnlyOnceInitConsoleCommand extends Command
{
    public function __construct(private OnlyOnceInit $onlyOnceInit)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln('Start');
        $isCreated = $this->onlyOnceInit->init();
        $this->onlyOnceInit->cleanOld();
        if ($isCreated) {
            $io->writeln('Init Succeed');
            return self::SUCCESS;
        }
        $io->writeln('Init Failed');

        return self::FAILURE;
    }
}
