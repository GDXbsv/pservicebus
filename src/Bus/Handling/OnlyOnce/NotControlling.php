<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling\OnlyOnce;


use GDXbsv\PServiceBus\Message\Message;

class NotControlling implements OnlyOnceControl, OnlyOnceInit
{
    public function continue(Message $message): bool
    {
        return true;
    }

    public function init(): bool
    {
        return true;
    }

    public function clean(Message $message): bool
    {
        return true;
    }

    public function cleanOld(): bool
    {
        return true;
    }
}
