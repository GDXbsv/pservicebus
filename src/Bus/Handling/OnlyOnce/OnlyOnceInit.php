<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling\OnlyOnce;

interface OnlyOnceInit
{
    public function init(): bool;
    public function cleanOld(): bool;
}
