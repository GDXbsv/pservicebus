<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus;

use GDXbsv\PServiceBus\Transport\Transport;

interface ConsumeBus
{
    /**
     * @psalm-return  \Traversable
     */
    public function consume(Transport $transport, int $limit = 0): \Traversable;

    public function stopConsume(): void;
}
