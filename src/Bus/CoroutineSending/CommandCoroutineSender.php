<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\CoroutineSending;

use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\Message;

final readonly class CommandCoroutineSender
{
    /**
     * @param \Generator<int, bool, Message<CommandOptions>|null, void> $generator
     */
    public function __construct(private \Generator $generator)
    {
    }

    public function send(object $message, CommandOptions $commandOptions = null): void
    {
        if ($commandOptions === null) {
            $commandOptions = CommandOptions::record();
        }
        $this->generator->send(new Message($message, $commandOptions));
    }

    public function finish(): void
    {
        $this->generator->send(null);
    }
}
