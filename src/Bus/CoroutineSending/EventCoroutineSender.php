<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\CoroutineSending;

use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;

final readonly class EventCoroutineSender
{
    /**
     * @param \Generator<int, bool, Message<EventOptions>|null, void> $generator
     */
    public function __construct(private \Generator $generator)
    {
    }

    public function publish(object $message, EventOptions $eventOptions = null): void
    {
        if ($eventOptions === null) {
            $eventOptions = EventOptions::record();
        }
        $this->generator->send(new Message($message, $eventOptions));
    }

    public function finish(): void
    {
        $this->generator->send(null);
    }
}
