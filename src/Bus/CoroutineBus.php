<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus;

use GDXbsv\PServiceBus\Bus\CoroutineSending\CommandCoroutineSender;
use GDXbsv\PServiceBus\Bus\CoroutineSending\EventCoroutineSender;

interface CoroutineBus
{
    public function sendCoroutine(): CommandCoroutineSender;

    public function publishCoroutine(): EventCoroutineSender;
}
