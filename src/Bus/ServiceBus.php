<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\CoroutineSending\CommandCoroutineSender;
use GDXbsv\PServiceBus\Bus\CoroutineSending\EventCoroutineSender;
use GDXbsv\PServiceBus\Bus\Handling\MessageHandleContext;
use GDXbsv\PServiceBus\Bus\Handling\MessageHandleInstruction;
use GDXbsv\PServiceBus\Bus\Middleware\InMiddleware;
use GDXbsv\PServiceBus\Bus\Middleware\OutMiddleware;
use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaHandling;
use GDXbsv\PServiceBus\Serializer\Serializer;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\EnvelopeCoroutineSender;
use GDXbsv\PServiceBus\Transport\Transport;
use Prewk\Result\Err;
use Prewk\Result\Ok;

/**
 * @psalm-import-type MessageClassMap from \GDXbsv\PServiceBus\Message\ScrapeExternals
 * @psalm-import-type MessageNameMap from \GDXbsv\PServiceBus\Message\ScrapeExternals
 */
final class ServiceBus implements Bus, ConsumeBus, CoroutineBus
{
    private bool $shouldStopConsume = false;
    /** @var \Closure(\Throwable, Message<MessageOptions>):void */
    private \Closure $errorHandler;
    /** @var list<OutMiddleware> */
    private array $outMiddlewares = [];
    /** @var list<InMiddleware> */
    private array $inMiddlewares = [];

    /**
     * @param array<string, Transport> $transportMap
     * @param array<class-string, list<MessageHandleInstruction>> $handlingInstructions
     * @param array<class-string, object> $handlerToObjectMap
     * @param MessageClassMap $messageInClassMap
     * @param MessageClassMap $messageOutClassMap
     * @param MessageNameMap $messageNameMap
     */
    public function __construct(
        private SagaHandling $sagaHandling,
        private Serializer $serializer,
        private array $transportMap,
        private array $handlingInstructions,
        private array $handlerToObjectMap,
        private array $messageInClassMap,
        private array $messageOutClassMap,
        private array $messageNameMap,
    ) {
        $this->errorHandler = $this->errorHandlerDefault();

        $intersect = array_intersect(array_keys($this->messageInClassMap), array_keys($this->messageOutClassMap));
        if (count($intersect) > 0) {
            $intersect = join(', ', $intersect);
            throw new \Exception("You have intersections between In and Out events: ({$intersect})");
        }
    }

    public function send(object $message, CommandOptions|null $commandOptions = null): void
    {
        $coroutine = $this->sendCoroutine();
        $coroutine->send($message, $commandOptions);
        $coroutine->finish();
    }

    public function publish(object $message, EventOptions|null $eventOptions = null): void
    {
        $coroutine = $this->publishCoroutine();
        $coroutine->publish($message, $eventOptions);
        $coroutine->finish();
    }

    public function sendCoroutine(): CommandCoroutineSender
    {
        /** @psalm-suppress ArgumentTypeCoercion we control what message option type in runtime */
        return new CommandCoroutineSender($this->coroutine());
    }

    public function publishCoroutine(): EventCoroutineSender
    {
        /** @psalm-suppress ArgumentTypeCoercion we control what message option type in runtime */
        return new EventCoroutineSender($this->coroutine());
    }

    /**
     * @return \Generator<int, bool, Message|null, void>
     * @psalm-suppress InvalidReturnType we sure we have this return type
     */
    private function coroutine(): \Generator
    {
        /** @var array<string, EnvelopeCoroutineSender> $sendings */
        $sendings = [];
        while (true) {
            $message = (yield true);
            if ($message === null) {
                foreach ($sendings as $sending) {
                    $sending->finish();
                }


                return;
            }
            foreach ($this->outMiddlewares as $middleware) {
                $middleware->before($message);
            }
            $payload = $message->payload;
            $messageType = $payload::class;
            if (!$this->checkCommandHasOneHandler($message)) {
                throw new \RuntimeException(
                    "Command '{$messageType}' must have exactly one handler. But we have: "
                    . (string)count($this->handlingInstructions[$messageType] ?? [])
                );
            }
            foreach ($this->messageEnvelopes($message) as [$envelope, $transport, $transportName]) {
                if (!array_key_exists($transportName, $sendings)) {
                    $sendings[$transportName] = $transport->sending();
                }
                $sending = $sendings[$transportName];
                $sending->send($envelope);
            }
            foreach ($this->outMiddlewares as $middleware) {
                $middleware->after();
            }
        }
    }

    public function consume(Transport $transport, int $limit = 0): \Traversable
    {
        $coroutineReceiver = $transport->receive($limit);

        while ($coroutineReceiver->valid()) {
            try {
                unset($message);
                $message = $this->envelopeForHandling($coroutineReceiver->getCurrentEnvelope());
                foreach ($this->inMiddlewares as $middleware) {
                    $middleware->before($message);
                }
                /** @var ?string $handlerName */
                $handlerName = $message->options->headers['handlerName'] ?? null;
                assert(isset($message->options->headers['type']));
                /** @var non-empty-string $type */
                $type = $message->options->headers['type'];
                if ($type === 'external') {
                    $this->redriveExternalInInternal($message);
                } elseif ($type === 'saga') {
                    /** @psalm-suppress ArgumentTypeCoercion we know it is always events */
                    $this->sagaHandling->handle($message);
                } else {
                    $object = $this->handlerToObjectMap[$handlerName] ?? null;
                    $methodName = $message->options->headers['handlerMethodName'] ?? null;
                    /** @var bool $isStaticMethod */
                    $isStaticMethod = $message->options->headers['isStaticMethod'] ?? false;
                    assert(is_string($methodName));
                    if ($isStaticMethod && is_string($handlerName)) {
                        /** @psalm-suppress InvalidStringClass */
                        $handlerName::{$methodName}(
                            $message->payload,
                            new MessageHandleContext($message->options, $this, $this)
                        );
                    } elseif ($object) {
                        /** @psalm-suppress MixedMethodCall */
                        $object->{$methodName}(
                            $message->payload,
                            new MessageHandleContext($message->options, $this, $this)
                        );
                    } else {
                        $methodName($message->payload, new MessageHandleContext($message->options, $this, $this));
                    }
                }
                $coroutineReceiver->sendResult(new Ok(null));
                foreach ($this->inMiddlewares as $middleware) {
                    $middleware->after(new Ok(null));
                }
                yield $message;
            } catch (\Throwable $throwable) {
                $result = new Err(new \Exception('Consume: ' . $throwable->getMessage(), 0, $throwable));
                $coroutineReceiver->sendResult($result);
                if (!isset($message)) {
                    $message = new Message(new \stdClass(), MessageOptions::record());
                }
                ($this->errorHandler)($throwable, $message);
            }

            if ($this->shouldStopConsume) {
                $coroutineReceiver->stop();
            }
        }
    }

    public function stopConsume(): void
    {
        $this->shouldStopConsume = true;
    }

    /**
     * @param callable(\Throwable, Message):void $errorHandler
     */
    public function setErrorHandler(callable $errorHandler): self
    {
        if (!($errorHandler instanceof \Closure)) {
            $errorHandler = \Closure::fromCallable($errorHandler);
        }
        /** @psalm-suppress MixedPropertyTypeCoercion We defined that closure has to have the correct type */
        $this->errorHandler = $errorHandler;

        return $this;
    }

    /**
     * @return \Closure(\Throwable, Message):void
     */
    private function errorHandlerDefault(): \Closure
    {
        return static function (\Throwable $_t, Message $_message): void {
            throw $_t;
        };
    }

    public function addInMiddleware(InMiddleware $middleware): void
    {
        $this->inMiddlewares[] = $middleware;
    }

    public function addOutMiddleware(OutMiddleware $middleware): void
    {
        $this->outMiddlewares[] = $middleware;
    }

    /**
     * @return \Traversable<array{Envelope, Transport, string}>
     */
    private function messageEnvelopes(Message $message): \Traversable
    {
        $payload = $message->payload;
        $messageType = $payload::class;
        $options = $message->options;

        //region REPLAY
        if (isset($options->headers['replay_type']) && $options->headers['replay_type'] === 'external') {
            $options = $options->withHeader('replay_type', 'replayed');
            goto external;
        }
        if (isset($options->headers['transportName'], $options->headers['handlerName'], $options->headers['handlerMethodName'],)) {
            assert(is_string($options->headers['handlerName']));
            assert(is_string($options->headers['transportName']));
            $transportName = $options->headers['transportName'];
            if (is_subclass_of($options->headers['handlerName'] ?: '', Saga::class)) {
                $options = $options
                    ->withHeader('saga', $options->headers['handlerName'])
                    ->withHeader('type', 'saga');
            } else {
                $options = $options->withHeader('type', $options::class);
            }
            yield [
                new Envelope(
                    $this->serializer->serialize($payload),
                    $options->retries,
                    $options->retriesTimeoutExpression,
                    $options->timeout->intervalSec,
                    $options
                        ->withHeader('name', $messageType)
                        ->toMap()
                ),
                $this->transportMap[$transportName],
                $transportName,
            ];

            return;
        }
        //endregion


        //region INTERNAL
        if (isset($this->handlingInstructions[$messageType])) {
            foreach ($this->handlingInstructions[$messageType] as $instruction) {
                if ($instruction->timeout) {
                    $options = $options->withTimeout($instruction->timeout);
                }
                if ($instruction->retries !== null) {
                    $options = $options->withRetries($instruction->retries);
                }
                if ($instruction->retriesTimeoutExpression !== null) {
                    $options = $options->withRetriesTimeoutExpression($instruction->retriesTimeoutExpression);
                }
                if (is_subclass_of(
                    ($instruction->handlerName !== null ? $instruction->handlerName : ''),
                    Saga::class
                )) {
                    $options = $options
                        ->withHeader('saga', $instruction->handlerName)
                        ->withHeader('type', 'saga');
                } else {
                    $options = $options->withHeader('type', $options::class);
                }
                yield [
                    new Envelope(
                        $this->serializer->serialize($payload),
                        $options->retries,
                        $options->retriesTimeoutExpression,
                        $options->timeout->intervalSec,
                        $options
                            ->withHeader('name', $messageType)
                            ->withHeader('handlerName', $instruction->handlerName)
                            ->withHeader('handlerMethodName', $instruction->handlerMethodName)
                            ->withHeader('isStaticMethod', $instruction->isStaticMethod)
                            ->withHeader('transportName', $instruction->transportName)
                            ->toMap()
                    ),
                    $this->transportMap[$instruction->transportName],
                    $instruction->transportName,
                ];
            }
        }
        //endregion

        //region EXTERNAL
        external:
        if (isset($this->messageOutClassMap[$messageType]) && !isset($options->headers['external_from_id'])) {

            [$transportName, $messageName] = $this->messageOutClassMap[$messageType];
            if (isset($this->messageInClassMap[$messageType])) {
                throw new \RuntimeException('You can send only ExternalOut messages to external.');
            }
            $options = $options
                ->withHeader('name', $messageName)
                ->withHeader('type', 'external');
            if (!isset($options->headers['transportName'])) {
                $options = $options
                    ->withHeader('transportName', $transportName);
            }
            yield [
                new Envelope(
                    $this->serializer->serialize($payload),
                    $options->retries,
                    $options->retriesTimeoutExpression,
                    $options->timeout->intervalSec,
                    $options->toMap()
                ),
                $this->transportMap[$transportName],
                $transportName
            ];
        }
        //endregion
    }

    private function envelopeForHandling(Envelope $envelope): Message
    {
        /** @var array{properties: mixed} $payloadRaw */
        $payloadRaw = $envelope->payload;

        /** @var array{name?:string, type:string, saga: string} $headers */
        $headers = $envelope->headers;
        assert(array_key_exists('name', $headers));
        /** @var class-string $payloadFqn */
        $payloadFqn = $headers['name'];
        if ($headers['type'] === 'external') {
            if (!isset($this->messageNameMap[$payloadFqn])) {
                throw new \RuntimeException("No type defined: '{$payloadFqn}'");
            }
            $payloadFqn = $this->messageNameMap[$payloadFqn];
        }

        return new Message(
            $this->serializer->deserialize(
                $payloadRaw,
                $payloadFqn
            ),
            MessageOptions::fromMap($envelope->headers)
        );
    }

    private function checkCommandHasOneHandler(Message $message): bool
    {
        if (!($message->options instanceof CommandOptions)) {
            return true;
        }
        if (count($this->handlingInstructions[$message->payload::class] ?? []) === 1) {
            return true;
        }
        return false;
    }

    private function redriveExternalInInternal(Message $message): void
    {
        $eventOptions = EventOptions::record($message->options->headers)
            ->withHeader('external', true)
            ->withHeader('external_from_id', $message->options->messageId->toString());
        $this->publish($message->payload, $eventOptions);
    }
}

