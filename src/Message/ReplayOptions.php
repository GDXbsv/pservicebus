<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @immutable
 * @psalm-immutable
 */
final class ReplayOptions extends MessageOptions
{
    protected static string $messageType = 'event-replay';
}
