<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

use GDXbsv\PServiceBus\Id;

/**
 * @immutable
 * @psalm-immutable
 */
class MessageOptions
{
    protected static string $messageType = '';

    private const DATE_FORMAT_STRING = 'Y-m-d\TH:i:s.uP';

    public TimeSpan $timeout;

    public int $retries = 3;
    public string $retriesTimeoutExpression = '60 * (2 ** retries_count)';

    /**
     * @param Id<static> $messageId
     * @param array<string, string|int|bool|null> $headers
     */
    final public function __construct(
        public Id $messageId,
        public \DateTimeImmutable $recordedOn,
        public array $headers
    ) {
        /** @psalm-suppress RiskyTruthyFalsyComparison this is fine fore now */
        if (empty($this->headers['message_type'])) {
            $this->headers['message_type'] = static::$messageType;
        }
        /** @psalm-suppress RiskyTruthyFalsyComparison this is fine fore now */
        if (static::$messageType === '' && !empty($this->headers['message_type']) && is_string(
                $this->headers['message_type']
            )) {
            static::$messageType = $this->headers['message_type'];
        }
        $this->headers['message_type'] = static::$messageType;
        $this->timeout = TimeSpan::fromSeconds(0);
    }

    final public static function record(array $headers = []): static
    {
        /**
         * @psalm-suppress MixedArgumentTypeCoercion we know that $headers type is correct
         * @psalm-suppress ArgumentTypeCoercion Id::new() is what we expect
         */
        return new static(Id::new(), new \DateTimeImmutable(), $headers);
    }

    /**
     * @param array<string, bool|int|string|null> $map
     */
    final public static function fromMap(array $map): static
    {
        /** @psalm-suppress RiskyTruthyFalsyComparison this is fine fore now */
        assert(
            !empty($map['message_id']) && is_string($map['message_id'])
            && !empty($map['recorded_on']) && is_string($map['recorded_on'])
        );
        $headers = $map;
        unset($headers['message_id'], $headers['recorded_on'], $headers['timeout_sec'], $headers['retries']);
        $dateTime = \DateTimeImmutable::createFromFormat(self::DATE_FORMAT_STRING, $map['recorded_on']);
        if ($dateTime === false) {
            throw new \RuntimeException("Can not convert to object date: '{$map['recorded_on']}'");
        }
        /** @psalm-suppress ArgumentTypeCoercion we know that type is correct */
        $self = new static(new Id($map['message_id']), $dateTime, $headers);

        /**
         * @psalm-suppress PossiblyInvalidArgument
         */
        $self->timeout = TimeSpan::fromSeconds($map['timeout_sec'] ?? 0);
        if (isset($map['retries']) && is_int($map['retries'])) {
            $self->retries = $map['retries'];
        }
        if (isset($map['retries_timeout_expression']) && is_string($map['retries_timeout_expression'])) {
            $self->retriesTimeoutExpression = $map['retries_timeout_expression'];
        }

        return $self;
    }

    final public function withHeader(string $name, string|int|bool|null $val): static
    {
        $new = clone $this;
        $new->headers[$name] = $val;
        return $new;
    }

    final public function withTimeout(TimeSpan $timeSpan): static
    {
        $new = clone $this;
        $new->timeout = $timeSpan;
        return $new;
    }

    final public function withRetries(int $retries): static
    {
        $new = clone $this;
        $new->retries = $retries;
        return $new;
    }

    final public function withRetriesTimeoutExpression(string $retriesTimeoutExpression): static
    {
        $new = clone $this;
        $new->retriesTimeoutExpression = $retriesTimeoutExpression;
        return $new;
    }

    /**
     * @return array<string, string|int|bool|null>
     */
    final public function toMap(): array
    {
        $map = $this->headers;
        $map['message_id'] = $this->messageId->toString();
        $map['recorded_on'] = $this->recordedOn->format(self::DATE_FORMAT_STRING);
        $map['timeout_sec'] = $this->timeout->intervalSec;
        $map['retries'] = $this->retries;
        $map['retries_timeout_expression'] = $this->retriesTimeoutExpression;

        return $map;
    }
}
