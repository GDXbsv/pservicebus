<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

use Prewk\Result;

final class EnvelopeCoroutineReceiver
{
    private bool $shouldStop = false;

    /**
     * @param \Generator<int, Envelope, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void> $generator
     */
    public function __construct(private readonly \Generator $generator)
    {
    }

    public function valid(): bool
    {
        return $this->generator->valid();
    }

    public function getCurrentEnvelope(): Envelope
    {
        $envelope = $this->generator->current();
        assert($envelope !== null, 'Check validity first with valid() method');
        return $envelope;
    }

    /**
     * @param Result\Ok<null, mixed>|Result\Err<mixed, \Exception> $result
     */
    public function sendResult(Result $result): void
    {
        $this->generator->send($result);
    }

    public function stop(): void
    {
        $this->shouldStop = true;
    }

    public function isStopDemanded(): bool
    {
        return $this->shouldStop;
    }
}
