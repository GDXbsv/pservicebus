<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

use GDXbsv\PServiceBus\Bus\ConsumeBus;
use Prewk\Result;

class InMemoryTransport implements Transport
{
    /** @var list<Envelope> */
    private array $envelopes = [];
    private ?ConsumeBus $bus;
    private bool $isDispatching = false;


    public function name(): string
    {
        return 'memory';
    }

    public function sending(): EnvelopeCoroutineSender
    {
        return new EnvelopeCoroutineSender($this->sendingCoroutine());
    }

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion Still do not know why
     */
    protected function sendingCoroutine(): \Generator
    {
        while (true) {
            $envelope = (yield);
            if (!$envelope) {
                break;
            }
            $type = $envelope->headers['type'] ?? '';
            if ($type === 'external') {
                break;
            }
            $this->envelopes[] = $envelope;
        }
        assert(isset($this->bus));
        if (!$this->isDispatching) {
            $this->isDispatching = true;
            try {
                foreach ($this->bus->consume($this) as $_message) {
                };
            } finally {
                $this->isDispatching = false;
            }
        }
    }

    public function receive(int $limit = 0): EnvelopeCoroutineReceiver
    {
        return new EnvelopeCoroutineReceiver($this->receiveCoroutine());
    }

    /**
     * @return \Generator<int, Envelope, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    private function receiveCoroutine(): \Generator
    {
        if (count($this->envelopes) === 0) {
            return;
        }
        while ($envelope = array_shift($this->envelopes)) {
            /** @var Result $result */
            $result = yield $envelope;
            if ($result->isErr()) {
                continue;
            }
            $result->unwrap();
        }
    }

    public function setBus(ConsumeBus $bus): void
    {
        $this->bus = $bus;
    }
}
