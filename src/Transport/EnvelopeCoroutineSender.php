<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

final readonly class EnvelopeCoroutineSender
{
    /**
     * @param \Generator<int, void, Envelope|null, void> $generator
     */
    public function __construct(private \Generator $generator)
    {
    }

    public function send(Envelope $envelope): void
    {
        $this->generator->send($envelope);
    }

    public function finish(): void
    {
        $this->generator->send(null);
    }
}
