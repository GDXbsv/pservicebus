<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

interface Transport
{
    /**
     * @return non-empty-string
     */
    public function name(): string;

    public function sending(): EnvelopeCoroutineSender;

    public function receive(int $limit = 0): EnvelopeCoroutineReceiver;
}
