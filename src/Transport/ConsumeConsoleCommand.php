<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

use GDXbsv\PServiceBus\Bus\ConsumeBus;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

use function pcntl_signal;

use const SIGHUP;
use const SIGINT;
use const SIGQUIT;
use const SIGTERM;
use const SIGWINCH;

/**
 * @psalm-type TRANSPORTS=array<string, Transport>
 */
#[AsCommand(name: 'p-service-bus:transport:consume', description: 'Get items from the queue and project them.')]
class ConsumeConsoleCommand extends Command
{
    private int $lastUsage = 0;
    private int $lastPeakUsage = 0;

    /**
     * @param TRANSPORTS $transports
     */
    public function __construct(private ConsumeBus $consumeBus, private array $transports)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                'transport',
                InputArgument::OPTIONAL,
                'Name of the transport.'
            )
            ->addOption(
                'limit',
                'l',
                InputOption::VALUE_REQUIRED,
                'How many queued items should be processed. 0 means infinity.)',
                '0'
            )
            ->addOption(
                'detect-leaks',
                null,
                InputOption::VALUE_NONE,
                'Output information about memory usage'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $limit = (int)$input->getOption('limit');
        if ($limit === 0) {
            $io->warning('Limit is "0". The command will not stop to work.');
        }
        $isDetectLeaks = (bool)$input->getOption('detect-leaks');

        $transport = $this->receiveTransportBus($input, $io);

        $signalHandler = function (int $_signo): void {
            $this->consumeBus->stopConsume();
        };

        pcntl_signal(SIGINT, $signalHandler);
        pcntl_signal(SIGHUP, $signalHandler);
        pcntl_signal(SIGQUIT, $signalHandler);
        pcntl_signal(SIGTERM, $signalHandler);
        # https://httpd.apache.org/docs/2.4/stopping.html#gracefulstop
        # https://github.com/docker-library/php/blob/master/8.0/buster/apache/Dockerfile
        # STOPSIGNAL SIGWINCH
        pcntl_signal(SIGWINCH, $signalHandler);

        foreach ($this->consumeBus->consume($transport, $limit) as $_domainMessage) {
            pcntl_signal_dispatch();
            // Print memory report if requested
            if ($isDetectLeaks) {
                // Gather memory info
                $peak = $this->getMemoryInfo(true);
                $curr = $this->getMemoryInfo(false);

                // Print report
                $output->writeln('== MEMORY USAGE ==');
                $output->writeln(
                    sprintf(
                        'Peak: %.02f KByte <%s>%s (%.03f %%)</%s>',
                        $peak['amount'] / 1024,
                        $peak['statusType'],
                        $peak['statusDescription'],
                        $peak['diffPercentage'],
                        $peak['statusType']
                    )
                );
                $output->writeln(
                    sprintf(
                        'Cur.: %.02f KByte <%s>%s (%.03f %%)</%s>',
                        $curr['amount'] / 1024,
                        $curr['statusType'],
                        $curr['statusDescription'],
                        $curr['diffPercentage'],
                        $curr['statusType']
                    )
                );
                $output->writeln('');

                // Unset variables to prevent unstable memory usage
                unset($peak, $curr);
            }
        }

        $output->writeln('<info>Succeed</info>');

        return 0;
    }

    private function receiveTransportBus(InputInterface $input, SymfonyStyle $io): Transport
    {
        /** @var mixed $busService */
        $busService = $input->getArgument('transport');
        if (!is_string($busService)) {
            $question = new ChoiceQuestion('Please select a transport.', array_keys($this->transports),);
            $question->setErrorMessage('Transport %s is invalid.');

            $busService = (string)$io->askQuestion($question);
        }

        if (!array_key_exists($busService, $this->transports)) {
            $transportsAvailable = implode(', ', array_keys($this->transports));
            throw new RuntimeException(
                "$busService transport is not defined. Defined transports: [$transportsAvailable]"
            );
        }

        return $this->transports[$busService];
    }

    /**
     * Get information about the current memory usage
     *
     * @param bool $peak True for peak usage, false for current usage
     *
     * @return array{amount: int, diff: int, diffPercentage: int|float, statusDescription: 'decreasing'|'increasing'|'stable', statusType: 'comment'|'error'|'info'}
     */
    private function getMemoryInfo(bool $peak = false): array
    {
        $lastUsage = ($peak) ? $this->lastPeakUsage : $this->lastUsage;
        $info = [];
        $info['amount'] = ($peak) ? memory_get_peak_usage() : memory_get_usage();
        $info['diff'] = $info['amount'] - $lastUsage;
        $info['diffPercentage'] = ($lastUsage === 0) ? 0 : (float)$info['diff'] / ((float)$lastUsage / 100.00);
        $info['statusDescription'] = 'stable';
        $info['statusType'] = 'info';

        if ($info['diff'] > 0) {
            $info['statusDescription'] = 'increasing';
            $info['statusType'] = 'error';
        } elseif ($info['diff'] < 0) {
            $info['statusDescription'] = 'decreasing';
            $info['statusType'] = 'comment';
        }

        // Update last usage variables
        if ($peak) {
            $this->lastPeakUsage = $info['amount'];
        } else {
            $this->lastUsage = $info['amount'];
        }

        return $info;
    }
}
