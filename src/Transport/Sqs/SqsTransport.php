<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sqs;

use Aws\Credentials\AssumeRoleCredentialProvider;
use Aws\Credentials\CredentialProvider;
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use Aws\Sts\StsClient;
use Enqueue\Dsn\Dsn;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\EnvelopeCoroutineReceiver;
use GDXbsv\PServiceBus\Transport\EnvelopeCoroutineSender;
use GDXbsv\PServiceBus\Transport\Transport;
use GDXbsv\PServiceBus\Transport\TransportSynchronisation;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Prewk\Result;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class SqsTransport implements TransportSynchronisation, Transport
{
    private const THROUGH_PUT_MAX_LIMIT = 10;

    private const PART_ONE = 0;
    private const PART_TWO = 1;
    private const SIZE_LIMIT_BYTES = 100000;
    private const ACCUMULATE = 30;
    private string $sqsEndpoint;
    private string $queueNameDlq;
    private ?string $allowFrom = null;
    private ExpressionLanguage $expressionLanguage;
    private EnvelopeCoroutineReceiver $receiverCurrent;

    /**
     * @param array<string,string> $tags
     */
    public function __construct(
        private string $queueName,
        private SqsClient $sqsClient,
        private string $sqsNamespace,
        private int $retries,
        private array $tags = [],
        private int $visibilityTimeout = 30,
        private int $waitSeconds = 20,
        private int $waitBetweenLoopsSeconds = 40,
        private int $messagesBatch = self::THROUGH_PUT_MAX_LIMIT,
        private bool $preload = true
    ) {
        $this->sqsEndpoint = (string)$sqsClient->getEndpoint();
        $this->queueNameDlq = $queueName . '_DL';
        $this->expressionLanguage = new ExpressionLanguage();
        if ($this->waitBetweenLoopsSeconds < 1) {
            throw new \RuntimeException('waitBetweenLoopsSeconds can not be less than 1.');
        }
        if ($this->messagesBatch < 1 || $this->messagesBatch > self::THROUGH_PUT_MAX_LIMIT) {
            throw new \RuntimeException('messagesBatch can be between 1 and 10');
        }
    }

    public static function ofDsn(
        string $dsnString,
        array $clientAdditionalConfig = [],
    ): self {
        $dsn = Dsn::parseFirst($dsnString);
        if (null === $dsn || $dsn->getSchemeProtocol() !== 'sqs') {
            throw new \InvalidArgumentException(
                'Malformed parameter "dsn". example: "sqs+http://key:secret@aws:4100/123456789012?region=eu-west-1&retries=3&visibilityTimeout=30&waitSeconds=20&waitBetweenLoopsSeconds=40&messagesBatch=10&preload=true&tags[name]=value&tags[name2]=value2&assume=arn%3Aaws%3Aiam%3A%3A123456789012%3Arole%2Fxaccounts3access&queue=QueueName"'
            );
        }

        $namespace = $dsn->getPath();
        assert(
            $namespace !== null,
            'error(-><-): sqs+http://key:secret@aws:4100->/123456789012<-?region=eu-west-1&queue=QueueName'
        );
        $queue = $dsn->getString('queue');
        assert(
            $queue !== null,
            'error(-><-): sqs+http://key:secret@aws:4100/123456789012?region=eu-west-1&->queue=QueueName<-'
        );
        $region = $dsn->getString('region');
        assert(
            $region !== null,
            'error(-><-): sqs+http://key:secret@aws:4100/123456789012?->region=eu-west-1<-&queue=QueueName'
        );
        $retries = ($dsn->getDecimal('retries') ?? 3);
        $visibilityTimeout = ($dsn->getDecimal('visibilityTimeout') ?? $dsn->getDecimal('visabilityTimeout') ?? 30);
        $waitSeconds = ($dsn->getDecimal('waitSeconds') ?? 20);
        $waitBetweenLoopsSeconds = ($dsn->getDecimal('waitBetweenLoopsSeconds') ?? 40);
        $messagesBatch = ($dsn->getDecimal('messagesBatch') ?? self::THROUGH_PUT_MAX_LIMIT);
        /** @var bool $preload */
        $preload = ($dsn->getBool('preload', true));
        /** @var array<string,string> $tags */
        $tags = $dsn->getArray('tags')->toArray();
        $user = $dsn->getUser();
        $password = $dsn->getPassword();

        $clientConfig = [
            'version' => 'latest',
            'region' => $region,
        ];
        if ($user !== null && $password !== null) {
            $clientConfig['credentials'] = [
                'key' => $user,
                'secret' => $password,
            ];
        } else {
            $provider = CredentialProvider::defaultProvider($clientConfig);
            $clientConfig['credentials'] = $provider;
        }
        $assume = $dsn->getString('assume', null);
        if ($assume !== null) {
            //https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials.html
            //https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials_provider.html#assumerole-provider
            $stsClientConfig = $clientConfig;
            $sessionName = "assume_session";

            $assumeRoleCredentials = new AssumeRoleCredentialProvider(
                [
                    'client' => new StsClient($stsClientConfig),
                    'assume_role_params' => [
                        'RoleArn' => $assume,
                        'RoleSessionName' => $sessionName,
                    ],
                ]
            );
            // To avoid unnecessarily fetching STS credentials on every API operation,
            // the memoize function handles automatically refreshing the credentials when they expire
            $providerAssume = CredentialProvider::memoize($assumeRoleCredentials);
            $clientConfig['credentials'] = $providerAssume;
        }
        $host = $dsn->getHost();
        if ($host !== null) {
            /**
             * @psalm-suppress PossiblyUndefinedIntArrayOffset
             * @var string $schemeExtension
             */
            $schemeExtension = $dsn->getSchemeExtensions()[0];
            $endpoint = sprintf(
                '%s://%s',
                $schemeExtension,
                $host
            );
            if ($dsn->getPort() !== null) {
                $endpoint .= ":{$dsn->getPort()}";
            }
            $clientConfig['endpoint'] = $endpoint;
        }

        return new self(
            $queue,
            new SqsClient(array_merge($clientConfig, $clientAdditionalConfig)),
            trim($namespace, '/'),
            $retries,
            $tags,
            $visibilityTimeout,
            $waitSeconds,
            $waitBetweenLoopsSeconds,
            $messagesBatch,
            $preload,
        );
    }

    public function name(): string
    {
        return 'aws_sqs';
    }

    public function setAllowFrom(?string $allowFrom): void
    {
        $this->allowFrom = $allowFrom;
    }

    public function arn(): string
    {
        return $this
            ->sqsClient
            ->getQueueArn($this->genUrl());
    }

    public function sending(): EnvelopeCoroutineSender
    {
        return new EnvelopeCoroutineSender($this->sendingCoroutine());
    }

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion
     */
    private function sendingCoroutine(): \Generator
    {
        $envelope = (yield);
        if ($envelope === null) {
            return;
        }
        $batchList = [];
        $promises = [
            self::PART_ONE => [],
            self::PART_TWO => [],
        ];
        $partCurrent = self::PART_ONE;
        while ($envelope) {
            $batchList[] = SqsEnvelope::ofEnvelope($envelope)->toSqsArray();
            if (self::THROUGH_PUT_MAX_LIMIT > \count($batchList)) {
                $envelope = (yield);
                continue;
            }
            if (self::SIZE_LIMIT_BYTES < strlen(json_encode($batchList))) {
                foreach ($batchList as $message) {
                    $promises[$partCurrent][] = $this->sendMessagesBatch([$message]);
                }
            } else {
                $promises[$partCurrent][] = $this->sendMessagesBatch($batchList);
            }
            $batchList = [];
            /** @psalm-suppress RedundantCondition */
            if ($partCurrent === self::PART_ONE && count($promises[self::PART_ONE]) >= self::ACCUMULATE) {
                $partCurrent = self::PART_TWO;
                if (count($promises[self::PART_TWO]) >= self::ACCUMULATE) {
                    Utils::all($promises[self::PART_TWO])->wait();
                    $promises[self::PART_TWO] = [];
                }
            }
            if ($partCurrent === self::PART_TWO && count($promises[self::PART_TWO]) >= self::ACCUMULATE) {
                $partCurrent = self::PART_ONE;
                if (count($promises[self::PART_ONE]) >= self::ACCUMULATE) {
                    Utils::all($promises[self::PART_ONE])->wait();
                    $promises[self::PART_ONE] = [];
                }
            }
            $envelope = (yield);
        }

        if (0 !== count($batchList)) {
            $promises[$partCurrent][] = $this->sendMessagesBatch($batchList);
        }

        Utils::all(\array_merge($promises[self::PART_TWO], $promises[self::PART_ONE]))->wait();

        $promises[self::PART_TWO] = [];
        $promises[self::PART_ONE] = [];
    }

    public function receive(int $limit = 0): EnvelopeCoroutineReceiver
    {
        $this->receiverCurrent = new EnvelopeCoroutineReceiver($this->receiveCoroutine($limit));
        return $this->receiverCurrent;
    }

    /**
     * @return \Generator<int, Envelope, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    private function receiveCoroutine(int $limit = 0): \Generator
    {
        $generator = $this->receiveRaw($limit);
        while ($generator->valid()) {
            /** @psalm-suppress PossiblyNullArgument according to type it not null */
            $result = (yield SqsEnvelope::ofSqs($generator->current())->toEnvelope());
            $generator->send($result);
        }
    }

    /**
     * @return \Generator<int, array{Attributes: array<string, int|string>, Body:string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>, MessageId: string, ReceiptHandle: string}, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    public function receiveRaw(int $limit = 0, EnvelopeCoroutineReceiver|null $receiver = null): \Generator
    {
        $receiver = $receiver ?? $this->receiverCurrent;
        $maxReceiveLimit = $this->messagesBatch;
        $messagesLeft = $limit;
        // if limit = 0, then reset to 10 (self::MAXLIMIT)
        // if $messagesLeft is 5, it will take 5. If it’s 100, then it will be 10 (self::MAXLIMIT).
        $messagesToProcessFind =
            static fn(int $left): int => ($limit === 0) ? $maxReceiveLimit : min($left, $maxReceiveLimit);
        $messagesToProcess = $messagesToProcessFind($messagesLeft);
        $messagesPromise = $this->receiveMessages($messagesToProcess);
        while (true) {
            [$messages, $messagesAt] = $messagesPromise->wait();
            $messagesLeft = ($messagesLeft === 0) ? $messagesLeft : $messagesLeft - count($messages);
            if ($limit === 0 && !$receiver->isStopDemanded() && count($messages) === 0) {
                $this->waitForContinue($this->waitBetweenLoopsSeconds, $receiver);
            }
            $messagesPromise = null;
            if (!$receiver->isStopDemanded()) {
                if (
                    $limit === 0
                    || (
                        $messagesLeft > 0
                        && count($messages) > 0
                        && count($messages) === $maxReceiveLimit
                    )
                ) {
                    $messagesToProcess = $messagesToProcessFind($messagesLeft);
                    $messagesPromise = $this->receiveMessages($messagesToProcess);
                }
            }
            foreach ($messages as $message) {
                /** @var Result $isSucceed */
                $isSucceed = (yield $message);

                if ($isSucceed->isErr()) {
                    $envelope = SqsEnvelope::ofSqs($message)->toEnvelope();
                    $this->extendVisibilityTimeout($message, $envelope);
                    continue;
                }
                $this->sqsClient->deleteMessage(
                    [
                        'QueueUrl' => $this->genUrl(),
                        'ReceiptHandle' => $message['ReceiptHandle'],
                    ]
                );
            }
            if ((time() - $messagesAt) > $this->visibilityTimeout) {
                $diffSec = time() - $messagesAt;
                trigger_error(
                    "Handling all the messages took longer than visibilityTimeout. "
                    . "visibilityTimeout = `$this->visibilityTimeout`; time to handle = `$diffSec`."
                    . "Adjust or/and (visibilityTimeout, messagesBatch, preload). Make your workload faster.",
                    E_USER_WARNING
                );
            }
            if ($messagesPromise === null) {
                break;
            }
        }
    }

    public function sync(): void
    {
        $result = $this->sqsClient->listQueues(
            [
                'QueueNamePrefix' => $this->queueName,
            ]
        );
        $checkNameInUrl = '/' . $this->queueName;
        /** @var array<string>|null $queueUrls */
        $queueUrls = $result->get('QueueUrls');
        if (is_array($queueUrls)) {
            foreach ($queueUrls as $queueUrl) {
                if (str_ends_with($queueUrl, $checkNameInUrl)) {
                    return;
                }
            }
        }

        // create dead letter queue
        /** @var array{QueueUrl: string} $deadLetterCreateResult */
        $deadLetterCreateResult = $this
            ->sqsClient
            ->createQueue(['QueueName' => $this->queueNameDlq, 'tags' => $this->tags]);

        $dlqArn = $this->sqsClient->getQueueArn($deadLetterCreateResult['QueueUrl']);
        // mostly need for tests (gtilab-ci problems)
        $parts = explode(':', $dlqArn);
        $parts[2] = 'sqs';
        $dlqArn = implode(':', $parts);
        if (!str_contains($dlqArn, $this->sqsClient->getRegion())) {
            $dlqArn = \str_replace('sqs:', 'sqs:' . $this->sqsClient->getRegion() . ':', $dlqArn);
        }
        if (str_contains($dlqArn, ':localhost:localstack:cloud:80:')) {
            $dlqArn = \str_replace(':localhost:localstack:cloud:80:', ':', $dlqArn);
        }

        $config = [
            'QueueName' => $this->queueName,
            'Attributes' => [
                'RedrivePolicy' => json_encode(
                    [
                        'deadLetterTargetArn' => $dlqArn,
                        'maxReceiveCount' => $this->retries,
                    ],
                    JSON_THROW_ON_ERROR
                ),
                'VisibilityTimeout' => $this->visibilityTimeout,
            ],
            'tags' => $this->tags,
        ];

        if ($this->allowFrom !== null) {
            $config['Attributes']['Policy'] = json_encode(
                [
                    'Version' => '2012-10-17',
                    'Id' => $this->arn() . '/SQSDefaultPolicy',
                    'Statement' =>
                        [
                            [
                                'Sid' => 'Sid' . (string)\random_int(0, 9999999999999),
                                'Effect' => 'Allow',
                                'Principal' => '*',
                                'Action' => 'SQS:SendMessage',
                                'Resource' => $this->arn(),
                                'Condition' =>
                                    [
                                        'ArnEquals' =>
                                            [
                                                'aws:SourceArn' => $this->allowFrom,
                                            ],
                                    ],
                            ],
                        ],
                ],
                JSON_THROW_ON_ERROR
            );
        }


        $this->sqsClient->createQueue($config);
    }

    private function sendMessagesBatch(array $entries, int $tryTo = 3): PromiseInterface
    {
        $sqsMessages = [
            'Entries' => $entries,
            'QueueUrl' => $this->genUrl(),
        ];

        $promise = $this->sqsClient->sendMessageBatchAsync($sqsMessages)->then(
            function (\Aws\Result $result) use ($entries, $tryTo): void {
                /** @var array<array{Code: string, Id: string, Message: string, SenderFault: string}> $failedQueues */
                $failedQueues = $result->toArray()['Failed'] ?? [];
                if (!empty($failedQueues)) {
                    if ($tryTo > 0) {
                        --$tryTo;
                        \usleep(\random_int(1000, 100000)); //1ms - 100ms
                        $this->sendMessagesBatch($entries, $tryTo)->wait();
                    } else {
                        $failedQueuesJson = json_encode($failedQueues, JSON_THROW_ON_ERROR);
                        $messagesForBatchJson = json_encode($entries, JSON_THROW_ON_ERROR);
                        throw new \RuntimeException(
                            "Could not send message to Queue during batch send queues inside Sqs/SendCoroutine.",
                            0,
                            new \Exception(
                                "FailedQueues: $failedQueuesJson",
                                0,
                                new \Exception("messagesForBatch: $messagesForBatchJson")
                            )
                        );
                    }
                }
            },
            function (\Throwable $error) use ($sqsMessages, $entries, $tryTo): void {
                if ($tryTo > 0) {
                    --$tryTo;
                    \usleep(\random_int(1000, 100000)); //1ms - 100ms
                    $this->sendMessagesBatch($entries, $tryTo)->wait();
                } else {
                    $sqsMessagesJson = json_encode($sqsMessages, JSON_THROW_ON_ERROR);

                    throw new \RuntimeException(
                        "Could not send messages to the queue.",
                        0,
                        new \Exception(
                            "Messages: $sqsMessagesJson",
                            0,
                            $error
                        )
                    );
                }
            }
        );

        Utils::queue()->run();

        return $promise;
    }

    /**
     * @return PromiseInterface<array{0:array<array{Attributes: array<string, string|int>, ReceiptHandle: string, Body: string,
     *   MessageId:string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>}>, 1: int}>
     */
    private function receiveMessages(int $limit): PromiseInterface
    {
        /**
         * @var Closure():PromiseInterface<array{0:array<array{Attributes: array<string, string|int>, ReceiptHandle: string, Body: string, MessageId:string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>}>, 1: int}> $messagePromise
         * @var array{Messages?: array<array{Attributes: array<string, string|int>, ReceiptHandle: string, Body: string, MessageId: string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>}>} $result
         */
        $messagePromise = fn(): PromiseInterface => $this->sqsClient->receiveMessageAsync(
            [
                'AttributeNames' => ['SentTimestamp', 'ApproximateReceiveCount'],
                'MaxNumberOfMessages' => $limit,
                'MessageAttributeNames' => ['All'],
                'QueueUrl' => $this->genUrl(),
                'WaitTimeSeconds' => $this->waitSeconds,
                'VisibilityTimeout' => $this->visibilityTimeout,
            ]
        )->then(
            function (\Aws\Result $result) {
                $timestamp = time();
                /**
                 * @var ?string $date
                 * @psalm-suppress MixedArrayAccess, PossiblyNullArrayAccess we expect AWS to have this data
                 */
                $date = $result['@metadata']['headers']['date'] ?? null;
                if (is_string($date)) {
                    $timestamp = strtotime($date);
                }

                return [$result['Messages'] ?? [], $timestamp];
            }
        );

        if ($this->preload) {
            $return = $messagePromise();
            Utils::queue()->run();
        } else {
            /**
             * @var PromiseInterface<array{0:array<array{Attributes: array<string, string|int>, ReceiptHandle: string, Body: string, MessageId:string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>}>, 1: int}> $return
             * @psalm-suppress UndefinedVariable Cannot find referenced variable $return
             */
            $return = new Promise(function () use (&$return, $messagePromise) {
                /**
                 * @var Promise $return
                 */
                $return->resolve($messagePromise()->wait());
            });
        }

        return $return;
    }

    /**
     * @param array{Attributes: array<string, string|int> , ReceiptHandle: string, ...} $message
     */
    private function extendVisibilityTimeout(array $message, Envelope $envelope): void
    {
        $retryNumber = (int)($message['Attributes']['ApproximateReceiveCount'] ?? 0);
        $receiptHandle = $message['ReceiptHandle'];

        if ($retryNumber < 1) {
            return;
        }

        $visibilityTimeout = (int)$this->expressionLanguage->evaluate(
            $envelope->retriesTimeoutExpression,
            [
                'retries_count' => $retryNumber,
            ]
        ); //seconds

        try {
            $this->sqsClient->changeMessageVisibility(
                [
                    'QueueUrl' => $this->genUrl(),
                    'ReceiptHandle' => $receiptHandle,
                    'VisibilityTimeout' => $visibilityTimeout,
                ]
            );
        } catch (SqsException $e) {
            if ('InvalidParameterValue' === $e->getAwsErrorCode()) {
                //skip this case. We do not really care. Iy happens time to time.
                return;
            }

            throw $e;
        }
    }

    private function genUrl(): string
    {
        return sprintf(
            '%s/%s/%s',
            $this->sqsEndpoint,
            $this->sqsNamespace,
            $this->queueName
        );
    }

    private function waitForContinue(int $seconds, EnvelopeCoroutineReceiver $receiver): void
    {
        for ($i = 0; $i < $seconds; ++$i) {
            sleep(1);
            if ($receiver->isStopDemanded()) {
                return;
            }
        }
    }
}
