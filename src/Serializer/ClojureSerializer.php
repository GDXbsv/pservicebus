<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Serializer;

use Doctrine\Instantiator\Instantiator;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\IdCollection;

class ClojureSerializer implements Serializer
{
    public function serialize(object $data): array
    {
        $hydrate = \Closure::bind(
            static function (object $object): array {
                /** @var array<scalar|object> $vars */
                $vars = get_object_vars($object);
                foreach ($vars as &$var) {
                    if (\is_object($var)) {
                        if ($var instanceof Id) {
                            $var = $var->toString();
                            continue;
                        }
                        if ($var instanceof IdCollection) {
                            $var = array_map(fn(Id $id) => $id->toString(), $var->ids);
                            continue;
                        }
                        if ($var instanceof \DateTimeInterface) {
                            $var = $var->format('Y-m-d\TH:i:s.uP');
                            continue;
                        }
                        throw new \Exception(
                            'You can\'t serialize objects, you gave ' . $var::class
                        );
                    }
                }

                return $vars;
            },
            null,
            $data::class
        );

        /**
         * @psalm-suppress PossiblyNullFunctionCall
         * @psalm-suppress MixedReturnStatement
         */
        return $hydrate($data);
    }

    /**
     * @template T of object
     * @param array{properties: mixed} $serializedData
     * @param class-string<T> $class
     * @return T
     * @throws \Exception
     * @psalm-suppress InvalidReturnType
     */
    public function deserialize(array $serializedData, string $class): object
    {
        /**
         * @var self $instance
         * @var class-string $class
         */
        $instance = (new Instantiator())->instantiate($class);
        $hydrate = \Closure::bind(
            function (array $payload, object $object): void {
                /** @var array<string, scalar|array<scalar>> $payload */
                foreach ($payload as $var => $value) {
                    $classReflection = new \ReflectionClass($object);
                    if (!$classReflection->hasProperty($var)) {
                        continue;
                    }
                    $property = $classReflection->getProperty($var);
                    if (!$property->hasType()) {
                        $class = $object::class;
                        throw new \RuntimeException(
                            "Can not deserialize property without type. Class: '{$class}' Property: '{$var}'."
                        );
                    }
                    /** @var \ReflectionNamedType $propertyType */
                    $propertyType = $property->getType();
                    $typeProperty = $propertyType->getName();
                    if (class_exists($typeProperty)) {
                        if ($typeProperty === IdCollection::class) {
                            /**
                             * @psalm-suppress ArgumentTypeCoercion we know that it should be an array
                             */
                            $object->{$var} = IdCollection::ofStrings($value);
                            continue;
                        }
                        /** @psalm-suppress MixedMethodCall */
                        $object->{$var} = new $typeProperty($value);
                        continue;
                    }
                    if (is_string($value)) {
                        $value = match ($typeProperty) {
                            'int' => (int)$value,
                            'float' => (float)$value,
                            default => $value
                        };
                    }

                    $object->{$var} = $value;
                }
            },
            null,
            $class
        );
        /** @psalm-suppress PossiblyNullFunctionCall */
        $hydrate($serializedData, $instance);

        /**
         * @psalm-suppress InvalidReturnStatement
         */
        return $instance;
    }
}
