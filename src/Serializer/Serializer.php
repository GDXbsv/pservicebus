<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Serializer;

interface Serializer
{
    public function serialize(object $data): array;

    /**
     * @template T of object
     * @param array{properties: mixed} $serializedData
     * @param class-string<T> $class
     * @return T
     */
    public function deserialize(array $serializedData, string $class): object;
}
