<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus;

use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;

interface Bus
{
    public function send(object $message, CommandOptions|null $commandOptions = null): void;
    public function publish(object $message, EventOptions|null $eventOptions = null): void;
}
