<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Schema\Comparator;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceControl;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceInit;
use GDXbsv\PServiceBus\Message\Message;

class DbalOnlyOnceControl implements OnlyOnceControl, OnlyOnceInit
{
    public function __construct(private Connection $connection, private string $tableName)
    {
    }

    public function continue(Message $message): bool
    {
        if (!isset($message->options->headers['handlerName'], $message->options->headers['handlerMethodName'])) {
            return true;
        }
        $handler = (string)$message->options->headers['handlerName'] . '::' . (string)$message->options->headers['handlerMethodName'];
        $builder = $this->connection->createQueryBuilder()
            ->insert($this->tableName)
            ->values([
                         'message_id' => ':message_id',
                         'handler' => ':handler',
                         'inserted_at' => ':inserted_at',
                     ])
            ->where('inserted_at < :inserted_at')
            ->setParameter('message_id', $message->options->messageId->toString())
            ->setParameter('handler', $handler)
            ->setParameter('inserted_at', new \DateTimeImmutable(), Types::DATETIMETZ_IMMUTABLE);
        try {
            $count = $builder->executeStatement();
        } catch (UniqueConstraintViolationException) {
            return false;
        }
        if (1 !== $count) {
            throw new \RuntimeException("Can not save message '{$message->options->messageId} $handler' in OnlyOnceControl.");
        }

        return true;
    }

    public function clean(Message $message): bool
    {
        if (!isset($message->options->headers['handlerName'], $message->options->headers['handlerMethodName'])) {
            return true;
        }
        $handler = (string)($message->options->headers['handlerName'] ?? '') . '::' . (string)($message->options->headers['handlerMethodName'] ?? '');
        $builder = $this->connection->createQueryBuilder()
            ->delete($this->tableName)
            ->where('message_id = :message_id AND handler = :handler_name')
            ->setParameter('message_id', $message->options->messageId->toString())
            ->setParameter('handler_name', $handler);
        try {
            $builder->executeStatement();
        } catch (UniqueConstraintViolationException) {
            return false;
        }

        return true;
    }

    public function cleanOld(): bool
    {
        $builder = $this->connection->createQueryBuilder()
            ->delete($this->tableName)
            ->where('inserted_at < :inserted_at')
            ->setParameter('inserted_at', new \DateTimeImmutable('-30 days'), Types::DATETIMETZ_IMMUTABLE);
        try {
            $builder->executeStatement();
        } catch (UniqueConstraintViolationException) {
            return false;
        }

        return true;
    }


    public function init(): bool
    {
        $schema = $this->connection->createSchemaManager()->introspectSchema();

        $table = $this->configureSchema($schema);

        if (null === $table) {
            return false;
        }

        $fromSchema = $this->connection->createSchemaManager()->introspectSchema();
        if (method_exists($this->connection->createSchemaManager(), 'createComparator')) {
            $comparator = $this->connection->createSchemaManager()->createComparator();
        } else {
            /** @psalm-suppress InternalMethod we have to use it for backward compatibility */
            $comparator = new Comparator();
        }
        $schemaDiff = $comparator->compareSchemas($fromSchema, $schema);
        $sql = $this->connection->getDatabasePlatform()->getAlterSchemaSQL($schemaDiff);
        /** @psalm-suppress PossiblyUndefinedIntArrayOffset */
        $this->connection->executeStatement($sql[0]);

        return true;
    }

    private function configureSchema(Schema $schema): ?Table
    {
        if ($schema->hasTable($this->tableName)) {
            return null;
        }

        return $this->configureTable($schema);
    }

    private function configureTable(Schema $schema): Table
    {
        $table = $schema->createTable($this->tableName);
        $table->addColumn('message_id', 'string', ['length' => 36]);
        $table->addColumn('handler', 'string', ['length' => 255]);
        $table->addColumn('inserted_at', Types::DATETIMETZ_IMMUTABLE);
        $table->setPrimaryKey(['message_id', 'handler']);

        return $table;
    }
}
