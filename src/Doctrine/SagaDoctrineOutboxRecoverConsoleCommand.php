<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\DBAL\Connection;
use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'p-service-bus:saga:doctrine:outbox:recover-messages', description: 'Send all leftover messages.')]
class SagaDoctrineOutboxRecoverConsoleCommand extends Command
{
    public function __construct(private Bus $bus, private Connection $connection, private string $tableOutbox)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln('Start');
        $io->progressStart();
        /** @var array{message_id: non-empty-string, message: non-empty-string} $row */
        foreach ($this->connection->iterateAssociative("SELECT * FROM {$this->tableOutbox}") as $row) {
            /** @var Message<EventOptions> $message */
            $message = unserialize($row['message']);
            $this->bus->publish($message->payload, $message->options);
            $this->connection->delete($this->tableOutbox, ['message_id' => $row['message_id']]);
            $io->progressAdvance();
        }
        $io->progressFinish();

        $io->writeln('Succeed');
        return self::SUCCESS;
    }
}
