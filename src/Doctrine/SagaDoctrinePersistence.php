<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceControl;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Saga\MessageSagaContext;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaFindDefinitions;
use GDXbsv\PServiceBus\Saga\SagaPersistence;

/**
 * @internal
 * @psalm-import-type FindersMap from \GDXbsv\PServiceBus\Saga\ScrapeFinders
 */
final class SagaDoctrinePersistence implements SagaPersistence
{
    private CoroutineBus $coroutineBus;

    /**
     * @param FindersMap $findInstructions
     * @param array<non-empty-string, object> $findToObjectMap
     */
    public function __construct(
        private EntityManagerInterface $em,
        private OnlyOnceControl $onlyOnceControl,
        private SagaFindDefinitions $sagaFinding,
        private string $tableOutbox,
        private array $findInstructions,
        private array $findToObjectMap
    ) {
    }

    public function retrieveSaga(Message $message, string $sagaType): ?Saga
    {
        $saga = null;
        $payload = $message->payload;
        $em = $this->em;
        $em->beginTransaction();
        if (!$this->onlyOnceControl->continue($message)) {
            $messageId = $message->options->messageId->toString();
            throw new \Exception("OnceControl: Message id already consumed: '{$messageId}'");
        }
        if (isset($this->findInstructions[$sagaType][$payload::class])) {
            $findInstruction = $this->findInstructions[$sagaType][$payload::class];
            assert(isset($this->findToObjectMap[$findInstruction->className]));
            /**
             * @var ?Saga $saga
             * @psalm-suppress MixedMethodCall
             */
            $saga = $this->findToObjectMap[$findInstruction->className]->{$findInstruction->methodName}(
                $payload,
                $message->options
            );
            if ($saga) {
                $em->lock($saga, LockMode::PESSIMISTIC_WRITE);

                return $saga;
            }
        }

        foreach ($this->sagaFinding->sagasForMessage($payload) as $finderDefinition) {
            if ($finderDefinition->sagaType !== $sagaType) {
                continue;
            }
            /** @var mixed $propertyValue */
            $propertyValue = ($finderDefinition->messageProperty)(
                $payload,
                new MessageSagaContext($message->options)
            );

            $qb = $em->createQueryBuilder();

            $mappins = $em->getClassMetadata($finderDefinition->sagaType)->fieldMappings;
            $type = $mappins[$finderDefinition->sagaProperty->getName()]['type'] ?? null;
            /** @var Saga|null $saga */
            $saga = $qb
                ->from($finderDefinition->sagaType, 'saga')
                ->select('saga')
                ->where($qb->expr()->eq("saga.{$finderDefinition->sagaProperty->getName()}", ':propertyValue'))
                ->setParameter(':propertyValue', $propertyValue, $type)
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getOneOrNullResult();
        }

        if ($saga === null) {
            foreach ($this->sagaFinding->sagaCreatorsForMessage($payload) as $creatorDefinition) {
                if ($creatorDefinition->sagaType !== $sagaType) {
                    continue;
                }
                $saga = ($creatorDefinition->sagaCreator)(
                    $payload,
                    new MessageSagaContext($message->options)
                );
                if ($saga) {
                    $em->persist($saga);
                }
            }
        }
        if ($saga !== null) {
            $em->lock($saga, LockMode::PESSIMISTIC_WRITE);
            return $saga;
        }

        return null;
    }

    public function saveSaga(Saga $saga, array $messages): void
    {
        /** @var EntityManager $em */
        $em = $this->em;
        if ($saga->completedAt === null) {
            $em->persist($saga);
        } else {
            $em->remove($saga);
        }
        $em->getUnitOfWork()->commit($saga);
        $this->saveMessagesInTable($messages);
        $em->commit();
        $coroutine = $this->coroutineBus->publishCoroutine();
        foreach ($messages as $message) {
            $coroutine->publish($message->payload, $message->options);
        }
        $coroutine->finish();
        $this->deleteMessagesFromTable($messages);
        $em->clear($saga::class);
    }

    public function cleanSaga(Saga $saga): void
    {
        /** @var EntityManager $em */
        $em = $this->em;
        try {
            $em->rollback();
        } catch (ConnectionException $e) {
            //skip no transaction exception
        }
        $em->clear();
    }

    public function setCoroutineBus(CoroutineBus $coroutineBus): void
    {
        $this->coroutineBus = $coroutineBus;
    }

    /**
     * @param list<Message<EventOptions>> $messages
     */
    protected function saveMessagesInTable(array $messages): void
    {
        if (count($messages) === 0) {
            return;
        }
        $values = [];
        $parameters = [];
        foreach ($messages as $message) {
            $values[] = "(?, ?)";
            $parameters[] = $message->options->messageId->toString();
            $parameters[] = serialize($message);
        }
        $this->em->getConnection()->executeStatement(
            sprintf(
                "INSERT INTO {$this->tableOutbox} (message_id, message) VALUES  %s",
                join(',', $values)
            ),
            $parameters
        );
    }

    /**
     * @param list<Message<EventOptions>> $messages
     */
    protected function deleteMessagesFromTable(array $messages): void
    {
        if (count($messages) === 0) {
            return;
        }
        $values = [];
        $parameters = [];
        foreach ($messages as $message) {
            $values[] = "?";
            $parameters[] = $message->options->messageId->toString();
        }
        $this->em->getConnection()->executeStatement(
            sprintf(
                "DELETE FROM {$this->tableOutbox} WHERE message_id IN (%s)",
                join(',', $values)
            ),
            $parameters
        );
    }
}
