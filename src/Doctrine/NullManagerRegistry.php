<?php

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\Persistence\AbstractManagerRegistry;
use Doctrine\Persistence\ObjectManager;

class NullManagerRegistry extends AbstractManagerRegistry
{
    public function __construct()
    {
        parent::__construct('', [], [], '', '', self::class);
    }

    protected function getService($name): ObjectManager
    {
        throw new \InvalidArgumentException('Method not implemented');
    }

    protected function resetService($name): void
    {
    }

    public function getAliasNamespace(string $alias): string
    {
        throw new \InvalidArgumentException('Method not implemented');
    }
}
