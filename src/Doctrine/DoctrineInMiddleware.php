<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Connections\PrimaryReadReplicaConnection;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use GDXbsv\PServiceBus\Bus\Middleware\InMiddleware;
use GDXbsv\PServiceBus\Message\Message;
use Prewk\Result;

class DoctrineInMiddleware implements InMiddleware
{
    private int $checkedSince;

    public function __construct(
        private ManagerRegistry $registry,
        private int $pingTimeout = 3600,
        private bool $rollbackTransaction = true,
    ) {
        $this->checkedSince = \time();
    }

    public function before(Message $message): void
    {
        static $isPingCall = false;

        if (!$isPingCall) {
            if ($this->pingTimeout > 0
                && (\time() - $this->checkedSince) > $this->pingTimeout
            ) {
                $isPingCall = true;

                /** @var array<string, Connection> $connections */
                $connections = $this->registry->getConnections();
                foreach ($connections as $name => $connection) {
                    if (!$connection->isConnected()) {
                        continue;
                    }
                    if ($connection instanceof PrimaryReadReplicaConnection) {
                        $connection->ensureConnectedToPrimary();
                        $this->ensureConnectionIsOpen($connection);
                        $this->ensureConnectionWithoutTransaction($name, $connection);
                        $connection->ensureConnectedToReplica();
                        $this->ensureConnectionIsOpen($connection);
                    } else {
                        $this->ensureConnectionIsOpen($connection);
                    }
                }
                $isPingCall = false;
                $this->checkedSince = \time();
            }
        }

        $ems = $this->registry->getManagers();
        foreach ($ems as $name => $em) {
            if ($em instanceof EntityManagerInterface && !$em->isOpen()) {
                $this->registry->resetManager($name);
            }
        }

        /** @var array<string, Connection> $connections */
        $connections = $this->registry->getConnections();
        foreach ($connections as $name => $connection) {
            $this->ensureConnectionWithoutTransaction($name, $connection);
            // Switch all connections back to replica
            if ($connection instanceof PrimaryReadReplicaConnection) {
                $connection->ensureConnectedToReplica();
            }
        }
    }

    public function after(Result $result): void
    {
        $ems = $this->registry->getManagers();
        foreach ($ems as $em) {
            $em->flush();
            $em->clear();
        }
    }

    private function ensureConnectionIsOpen(Connection $connection): void
    {
        if ($connection->isConnected()) {
            try {
                $connection->executeQuery($connection->getDatabasePlatform()->getDummySelectSQL());
            } catch (DBALException) {
                $connection->close();
            }
        }
    }

    private function ensureConnectionWithoutTransaction(string $name, Connection $connection): void
    {
        if (!$this->rollbackTransaction) {
            return;
        }
        if ($connection->isTransactionActive()) {
            try {
                $connection->rollBack();
            } catch (\Throwable) {
                // we should never go here but for a case we do not care
            }
        }
    }
}
