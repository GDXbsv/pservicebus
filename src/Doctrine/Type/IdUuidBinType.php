<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Exception\InvalidType;
use Doctrine\DBAL\Types\Exception\ValueNotConvertible;
use Doctrine\DBAL\Types\Type;
use GDXbsv\PServiceBus\Id;
use Ramsey\Uuid\Uuid;

final class IdUuidBinType extends Type
{
    const NAME = 'id_bin';

    /**
     * @param mixed $value
     * @return Id|null
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Id
    {
        if ($value instanceof Id || null === $value) {
            return $value;
        }

        if (!\is_string($value)) {
            $this->throwInvalidType($value);
        }
        if (Uuid::isValid($value)) {
            return new Id($value);
        }

        try {
            return new Id(Uuid::fromBytes($value)->toString());
        } catch (\InvalidArgumentException $e) {
            $this->throwValueNotConvertible($value, $e);
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param mixed $value
     *
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if ($value === null || $value === '') {
            return null;
        }


        $isNative = $this->hasNativeGuidType($platform);

        if ($value instanceof Id) {
            if ($isNative) {
                return Uuid::fromString($value->toString())->toString();
            } else {
                return Uuid::fromString($value->toString())->getBytes();
            }
        }

        if (!\is_string($value)) {
            $this->throwInvalidType($value);
        }

        try {
            if ($isNative) {
                return Uuid::fromString($value)->toString();
            } else {
                return Uuid::fromString($value)->getBytes();
            }
        } catch (\InvalidArgumentException $e) {
            $this->throwValueNotConvertible($value, $e);
        }
    }

    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        if ($this->hasNativeGuidType($platform)) {
            return $platform->getGuidTypeDeclarationSQL($column);
        }

        return $platform->getBinaryTypeDeclarationSQL([
                                                          'length' => 16,
                                                          'fixed' => true,
                                                      ]);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    private function throwInvalidType(mixed $value): never
    {
        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', 'string', IdUuidBinType::class]
        );
    }

    private function throwValueNotConvertible(mixed $value, \Throwable $previous): never
    {
        throw ConversionException::conversionFailed($value, $this->getName(), $previous);
    }

    private function hasNativeGuidType(AbstractPlatform $platform): bool
    {
        return $platform->getGuidTypeDeclarationSQL([]) !== $platform->getStringTypeDeclarationSQL(
                ['fixed' => true, 'length' => 36]
            );
    }
}
