<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use Doctrine\Persistence\ManagerRegistry;
use GDXbsv\PServiceBus\Bus\ConsumeBus;
use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\NotControlling;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceControl;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceInit;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceInitConsoleCommand;
use GDXbsv\PServiceBus\Bus\Handling\ScrapeHandlers;
use GDXbsv\PServiceBus\Bus\Middleware\InMiddleware;
use GDXbsv\PServiceBus\Bus\Middleware\OutMiddleware;
use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBus\Doctrine\DbalOnlyOnceControl;
use GDXbsv\PServiceBus\Doctrine\DoctrineInMiddleware;
use GDXbsv\PServiceBus\Doctrine\NullManagerRegistry;
use GDXbsv\PServiceBus\Doctrine\SagaDoctrineOutboxInitConsoleCommand;
use GDXbsv\PServiceBus\Doctrine\SagaDoctrineOutboxRecoverConsoleCommand;
use GDXbsv\PServiceBus\Doctrine\SagaDoctrinePersistence;
use GDXbsv\PServiceBus\Doctrine\SagaDoctrineSubscriber;
use GDXbsv\PServiceBus\Doctrine\Type\IdJsonCollectionType;
use GDXbsv\PServiceBus\Doctrine\Type\IdType;
use GDXbsv\PServiceBus\Doctrine\Type\IdUuidBinType;
use GDXbsv\PServiceBus\Doctrine\Type\IdUuidType;
use GDXbsv\PServiceBus\Message\ExternalIn;
use GDXbsv\PServiceBus\Message\ExternalOut;
use GDXbsv\PServiceBus\Message\Replay\ReplayConsoleCommand;
use GDXbsv\PServiceBus\Message\Replay\Replaying;
use GDXbsv\PServiceBus\Message\Replay\ScrapeReplays;
use GDXbsv\PServiceBus\Message\ScrapeExternals;
use GDXbsv\PServiceBus\Saga\InMemoryPersistence;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaHandling;
use GDXbsv\PServiceBus\Saga\SagaMapper;
use GDXbsv\PServiceBus\Saga\ScrapeFinders;
use GDXbsv\PServiceBus\Serializer\ClojureSerializer;
use GDXbsv\PServiceBus\Transport\ConsumeConsoleCommand;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBus\Transport\Transport;
use GDXbsv\PServiceBus\Transport\TransportSyncConsoleCommand;
use GDXbsv\PServiceBus\Transport\TransportSynchronisation;
use Symfony\Component\Console\Application;

/**
 * @internal
 */
final class Setup
{
    protected SetupConfig $setupConfig;

    protected Bus $bus;
    protected ConsumeBus $consumeBus;
    protected CoroutineBus $coroutineBus;
    protected SagaHandling $sagaHandling;
    protected Replaying $replaying;
    protected ?EntityManager $entityManager = null;
    protected ?ManagerRegistry $registry = null;
    /** @var list<class-string<Saga>> */
    protected array $sagaClasses;
    protected string $tableOutbox;
    protected OnlyOnceControl $onlyOnce;
    protected OnlyOnceInit $onlyOnceInit;
    /** @var TransportSynchronisation[] */
    private array $transportSynchronisations = [];
    /** @var array<string,Transport> */
    private array $transports = [];

    /**
     * @param list<class-string> $classesWithHandlers
     * @param array<string, Transport> $transportsByNameMap
     * @param array<class-string, object> $handlerToObjectMap
     * @param list<class-string> $messageClasses
     * @param list<class-string> $replayClasses
     * @param array<non-empty-string, object> $replayToObjectMap
     * @param list<class-string> $sagaFindClasses
     * @param array<non-empty-string, object> $sagaFindToObjectMap
     * @param array<OutMiddleware> $outMiddlewares
     * @param array<InMiddleware> $inMiddlewares
     * @param class-string<ServiceBus> $busDecorator
     */
    public function __construct(
        array $classesWithHandlers,
        array $transportsByNameMap,
        array $handlerToObjectMap,
        array $messageClasses = [],
        array $replayClasses = [],
        array $replayToObjectMap = [],
        array $sagaFindClasses = [],
        array $sagaFindToObjectMap = [],
        array $outMiddlewares = [],
        array $inMiddlewares = [],
        string $busDecorator = null
    ) {
        $this->setupConfig = new SetupConfig(
            $classesWithHandlers,
            $transportsByNameMap,
            $handlerToObjectMap,
            $messageClasses,
            $replayClasses,
            $replayToObjectMap,
            $sagaFindClasses,
            $sagaFindToObjectMap,
            $outMiddlewares,
            $inMiddlewares,
            $busDecorator
        );
        $onlyOnce = new NotControlling();
        $this->onlyOnceInit = $onlyOnce;
        $this->onlyOnce = $onlyOnce;
    }

    public function build(): void
    {
        $this->transports = $this->setupConfig->transportsByNameMap;
        $serializer = new ClojureSerializer();

        /** @var list<class-string<Saga>> $sagas */
        $sagas = array_filter(
            $this->setupConfig->classesWithHandlers,
            fn(string $className) => is_subclass_of($className, Saga::class)
        );
        $this->sagaClasses = $sagas;
        $sagaMapper = new SagaMapper($sagas);
        $sagaFindInstructions = ScrapeFinders::fromClasses($this->setupConfig->sagaFindClasses);
        $sagaPersistence = new InMemoryPersistence(
            $sagaMapper,
            $sagaFindInstructions,
            $this->setupConfig->sagaFindToObjectMap
        );
        if ($this->entityManager) {
            $sagaPersistence = new SagaDoctrinePersistence(
                $this->entityManager,
                $this->onlyOnce,
                $sagaMapper,
                $this->tableOutbox,
                $sagaFindInstructions,
                $this->setupConfig->sagaFindToObjectMap
            );
        }
        $this->sagaHandling = new SagaHandling($sagaPersistence);
        [$messageClassMapOut, $messageNameMapOut] = ScrapeExternals::fromClasses(
            $this->setupConfig->messageClasses,
            ExternalOut::class
        );
        [$messageClassMapIn, $messageNameMapIn] = ScrapeExternals::fromClasses(
            $this->setupConfig->messageClasses,
            ExternalIn::class
        );

        $handlingInstructions = ScrapeHandlers::fromClasses($this->setupConfig->classesWithHandlers);
        $bus = new ServiceBus(
            $this->sagaHandling,
            $serializer,
            $this->setupConfig->transportsByNameMap,
            $handlingInstructions,
            $this->setupConfig->handlerToObjectMap,
            $messageClassMapIn,
            $messageClassMapOut,
            array_merge($messageNameMapOut, $messageNameMapIn),
        );
        foreach ($this->setupConfig->outMiddlewares as $middleware) {
            $bus->addOutMiddleware($middleware);
        }
        foreach ($this->setupConfig->inMiddlewares as $middleware) {
            $bus->addInMiddleware($middleware);
        }
        if ($this->setupConfig->busDecorator) {
            $bus = new $this->setupConfig->busDecorator($bus, $bus, $bus);
        }
        $this->bus = $bus;
        $this->consumeBus = $bus;
        $this->coroutineBus = $bus;

        $sagaPersistence->setCoroutineBus($this->bus);
        foreach ($this->setupConfig->transportsByNameMap as $transport) {
            if ($transport instanceof InMemoryTransport) {
                $transport->setBus($this->bus);
            }
        }

        $replays = ScrapeReplays::fromClasses($this->setupConfig->replayClasses);
        $this->replaying = new Replaying($this->coroutineBus, $replays, $this->setupConfig->replayToObjectMap);
    }

    /**
     * @param array<array-key, string> $paths
     * @param array<string, mixed> $connectionConfig
     */
    public function addDoctrine(array $paths, string $tableOutbox, string $tableOnlyOnce, array $connectionConfig): void
    {
        $isDevMode = true;
//        $proxyDir = __DIR__ . '/../var/test/doctrineProxy';
        $proxyDir = null;
        $cache = null;
        $config = ORMSetup::createAttributeMetadataConfiguration(
            $paths,
            $isDevMode,
            $proxyDir,
            $cache
        );

        $eventManager = new EventManager();
        $eventManager->addEventSubscriber(new SagaDoctrineSubscriber());

        /** @psalm-suppress ArgumentTypeCoercion $connection */
        $connection = DriverManager::getConnection($connectionConfig, $config, $eventManager);
        $this->entityManager = new EntityManager($connection, $config, $eventManager);
        $this->registry = new NullManagerRegistry();
        $this->tableOutbox = $tableOutbox;
        $onlyOnce = new DbalOnlyOnceControl($this->entityManager->getConnection(), $tableOnlyOnce);
        $this->onlyOnce = $onlyOnce;
        $this->onlyOnceInit = $onlyOnce;

        $inMiddlewares = [];
        foreach ($this->setupConfig->inMiddlewares as $middleware) {
            $inMiddlewares[] = $middleware;
        }
        $inMiddlewares[] = new DoctrineInMiddleware($this->registry);
        $this->setupConfig->inMiddlewares = $inMiddlewares;

        Type::hasType('id') ?: Type::addType('id', IdType::class);
        Type::hasType('id_uuid') ?: Type::addType('id_uuid', IdUuidType::class);
        Type::hasType('id_bin') ?: Type::addType('id_bin', IdUuidBinType::class);
        Type::hasType('id_json_collection') ?: Type::addType('id_json_collection', IdJsonCollectionType::class);
    }

    /**
     * @param list<class-string> $sagaFindClasses
     * @param array<non-empty-string, object> $sagaFindToObjectMap
     */
    public function addSagaFinders(array $sagaFindClasses = [], array $sagaFindToObjectMap = [],): void
    {
        $this->setupConfig->sagaFindClasses = array_merge($this->setupConfig->sagaFindClasses, $sagaFindClasses);
        $this->setupConfig->sagaFindToObjectMap = array_merge(
            $this->setupConfig->sagaFindToObjectMap,
            $sagaFindToObjectMap
        );
    }

    public function addTransportSync(TransportSynchronisation $transportSynchronisation): void
    {
        $this->transportSynchronisations[] = $transportSynchronisation;
    }

    public function getApplication(): Application
    {
        $app = new Application();
        $app->add(new TransportSyncConsoleCommand($this->transportSynchronisations));
        $app->add(new OnlyOnceInitConsoleCommand($this->onlyOnceInit));
        $app->add(new ConsumeConsoleCommand($this->consumeBus, $this->transports,));
        $app->add(new InitConsoleCommand());
        $app->add(new ReplayConsoleCommand($this->replaying));
        if ($this->entityManager) {
            $app->add(
                new SagaDoctrineOutboxRecoverConsoleCommand(
                    $this->bus,
                    $this->entityManager->getConnection(),
                    $this->tableOutbox
                )
            );
            $app->add(
                new SagaDoctrineOutboxInitConsoleCommand(
                    $this->entityManager->getConnection(),
                    $this->tableOutbox
                )
            );
        }
        return $app;
    }

    public function getBus(): Bus
    {
        return $this->bus;
    }

    public function getEntityManager(): ?EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @return list<class-string<Saga>>
     */
    public function getSagaClasses(): array
    {
        return $this->sagaClasses;
    }
}

/**
 * @internal
 */
class SetupConfig
{
    /**
     * @param list<class-string> $classesWithHandlers
     * @param array<string, Transport> $transportsByNameMap
     * @param array<class-string, object> $handlerToObjectMap
     * @param list<class-string> $messageClasses
     * @param list<class-string> $replayClasses
     * @param array<non-empty-string, object> $replayToObjectMap
     * @param list<class-string> $sagaFindClasses
     * @param array<non-empty-string, object> $sagaFindToObjectMap
     * @param iterable<OutMiddleware> $outMiddlewares
     * @param iterable<InMiddleware> $inMiddlewares
     * @param class-string<ServiceBus> $busDecorator
     */
    public function __construct(
        public array $classesWithHandlers,
        public array $transportsByNameMap,
        public array $handlerToObjectMap,
        public array $messageClasses,
        public array $replayClasses,
        public array $replayToObjectMap,
        public array $sagaFindClasses,
        public array $sagaFindToObjectMap,
        public iterable $outMiddlewares,
        public iterable $inMiddlewares,
        public ?string $busDecorator
    ) {
    }
}
