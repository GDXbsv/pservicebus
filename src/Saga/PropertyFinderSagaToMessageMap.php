<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

/**
 * @immutable
 * @psalm-immutable
 */
final class PropertyFinderSagaToMessageMap extends CorrelationSagaToMessageMap
{
    /**
     * @param class-string<Saga> $sagaType
     * @param \Closure(object):\GDXbsv\PServiceBus\Id<Saga> $messageProperty
     */
    public function __construct(string $sagaType, \ReflectionProperty $sagaProperty, \Closure $messageProperty)
    {
        $this->sagaProperty = $sagaProperty;
        $this->messageProperty = $messageProperty;
        $this->sagaType = $sagaType;
    }

    public function createSagaFinderDefinition(): SagaFinderDefinition
    {
        return new SagaFinderDefinition($this->sagaType, $this->sagaProperty, $this->messageProperty);
    }
}
