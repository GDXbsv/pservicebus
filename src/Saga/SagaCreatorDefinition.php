<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class SagaCreatorDefinition
{
    /**
     * @param class-string<Saga> $sagaType
     * @param \Closure(object, \GDXbsv\PServiceBus\Saga\MessageSagaContext):?Saga $sagaCreator
     */
    public function __construct(
        public string $sagaType,
        public \Closure $sagaCreator
    ) {
    }
}
