<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

/**
 * @immutable
 * @psalm-immutable
 */
final class SagaFindInstruction
{
    public function __construct(
        public string $className,
        public string $methodName,
    ) {
    }

    /**
     * @param array{className: string, methodName: string} $data
     * @return static
     */
    public static function __set_state(array $data): self
    {
        return new self(
            $data['className'],
            $data['methodName'],
        );
    }
}
