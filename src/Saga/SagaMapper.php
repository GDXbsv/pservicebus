<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class SagaMapper implements SagaMessageFindingConfiguration, SagaCreateByMessageConfiguration, SagaFindDefinitions
{
    /** @var array<class-string, array<string, SagaToMessageMap>> */
    private array $findMappings = [];
    /** @var array<class-string, array<string, SagaCreateToMessageMap>> */
    private array $createMappings = [];

    /**
     * @param list<class-string<Saga>> $sagasClasses
     */
    public function __construct(array $sagasClasses)
    {
        $this->scanSagas($sagasClasses);
    }

    public function configureCreateMapping(string $sagaType, \Closure $sagaCreator): void
    {
        $reflection = new \ReflectionFunction($sagaCreator);
        $arguments = $reflection->getParameters();
        assert(isset($arguments[0]));
        /** @var \ReflectionNamedType $type */
        $type = $arguments[0]->getType();
        /** @var class-string $messageType */
        $messageType = $type->getName();

        $this->createMappings[$messageType][$sagaType] = new SagaCreatorToMessageMap($sagaType, $sagaCreator);
    }

    public function configureFindMapping(
        string $sagaType,
        \ReflectionProperty $sagaProperty,
        \Closure $messageProperty
    ): void {
        $reflection = new \ReflectionFunction($messageProperty);
        $arguments = $reflection->getParameters();
        assert(isset($arguments[0]));
        /** @var \ReflectionNamedType $type */
        $type = $arguments[0]->getType();
        /** @var class-string $messageType */
        $messageType = $type->getName();

        $this->findMappings[$messageType][$sagaType] = new PropertyFinderSagaToMessageMap($sagaType, $sagaProperty, $messageProperty);
    }

    public function sagasForMessage(object $message): \Traversable
    {
        $finders = $this->findMappings[$message::class] ?? [];
        if (count($finders) === 0) {
            return;
        }

        foreach ($finders as $finder) {
            yield $finder->createSagaFinderDefinition();
        }
    }


    public function sagaCreatorsForMessage(object $message): \Traversable
    {
        $finders = $this->createMappings[$message::class] ?? [];
        if (count($finders) === 0) {
            return;
        }

        foreach ($finders as $finder) {
            yield $finder->createSagaCreatorDefinition();
        }
    }

    /**
     * @param list<class-string<Saga>> $sagasClasses
     */
    private function scanSagas(array $sagasClasses): void
    {
        foreach ($sagasClasses as $sagaClass) {
            $sagaCreateMapper = new SagaCreateMapper($sagaClass, $this);
            $sagaClass::configureHowToCreateSaga($sagaCreateMapper);
            $sagaPropertyMapper = new SagaPropertyMapper($sagaClass, $this);
            $sagaClass::configureHowToFindSaga($sagaPropertyMapper);
        }
    }
}
