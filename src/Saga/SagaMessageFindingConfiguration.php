<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

interface SagaMessageFindingConfiguration
{
    /**
     * @param \Closure(object):\GDXbsv\PServiceBus\Id<Saga> $messageProperty
     * @param class-string<Saga>  $sagaType
     */
    public function configureFindMapping(
        string $sagaType,
        \ReflectionProperty $sagaProperty,
        \Closure $messageProperty
    ): void;
}
