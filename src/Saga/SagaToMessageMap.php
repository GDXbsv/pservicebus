<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

/**
 * @immutable
 * @psalm-immutable
 */
abstract class SagaToMessageMap
{
    /** @var class-string<Saga> */
    protected string $sagaType;

    abstract function createSagaFinderDefinition(): SagaFinderDefinition;
}
